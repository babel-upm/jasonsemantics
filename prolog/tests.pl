%% Tests for the jason interpreter, including running and checking the robot example.
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)
%%
%% Example code, except for the robot, are located in examples.pl
%%

outLevel(error).

:- begin_tests(jason).
:- use_module(library(lists)).

test(agent1) :-
    outLevel(Level),
    setof(Actions,run_agent(defag1,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(CollectedActions == [[action('.print'(120))]]).
    
test(agent2) :-
    outLevel(Level),
    setof(Actions,run_agent(defag2,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(CollectedActions == [[action('.print'(120))]]).
    
test(agent3) :-
    outLevel(Level),
    setof(Actions,run_agent(defag3,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(permutation(CollectedActions,[[action('.print'(5))],[action('.print'(0))]])).
    
test(system1) :-
    outLevel(Level),
    setof(Actions,FinalSys^run_system(defsys1,FinalSys,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(CollectedActions == [[action('.print'(120))]]).

test(system2) :-
    outLevel(Level),
    setof(Actions,FinalSys^run_system(defsys2,FinalSys,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(CollectedActions == [[action('.print'(120))]]).
    
test(system3) :-
    outLevel(Level),
    setof(Actions,FinalSys^run_system(defsys3,FinalSys,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(permutation(CollectedActions,[[action('.print'(5))],[action('.print'(0))]])).

test(system4) :-
    outLevel(Level),
    run_system(defsys4,_,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    assertion(Actions==[action('.print'(hello))]),
    !.

test(system5) :-
    outLevel(Level),
    run_system(defsys5,_,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    assertion(Actions==[action('.print'('hello')),action('.print'('hello'))]),
    !.

test(system6) :-
    outLevel(Level),
    run_system(defsys6,_,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    assertion(Actions==[action('.print'(the_world_is_round))]),
    !.

test(system7) :-
    outLevel(Level),
    run_system(defsys7,_,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    assertion(Actions==[action('.print'(hello))]),
    !.

test(system_ping1) :-
    outLevel(Level),
    run_system(defsys14,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    FineAction = action('.print'(fine)),
    assertion(Actions==[FineAction]),
    !.

test(system_ping1_sup) :-
    outLevel(Level),
    run_system(defsys14_sup,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    FineAction = action('.print'(fine)),
    assertion(forall(member(Action,Actions),Action==FineAction)),
    !.

%% Tests that a supervisor will restart an agent exactly N=3 times.
test(system_3restarts) :-
    outLevel(Level),
    run_system(defsys16,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    HelloAction = action('.print'(hello)),
    assertion(Actions==[HelloAction,HelloAction,HelloAction,HelloAction]),
    !.

%% Tests that a supervisor uses restart strategies well
test(system_restarts_rest_for_all) :-
    outLevel(Level),
    run_system(defsys17_1_rest_for_all,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    Hello1Action = action('.print'(hello(agent1))),
    Hello2Action = action('.print'(hello(agent2))),
    Hello3Action = action('.print'(hello(agent3))),
    count(Hello1Action,Actions,NumRestartsAgent1),
    count(Hello2Action,Actions,NumRestartsAgent2),
    count(Hello3Action,Actions,NumRestartsAgent3),
    assertion({NumRestartsAgent1,NumRestartsAgent2,NumRestartsAgent3}=={2,2,2}),
    !.

%% Tests that a supervisor uses restart strategies well
test(system_restarts_rest_for_one) :-
    outLevel(Level),
    run_system(defsys17_1_rest_for_one,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    Hello1Action = action('.print'(hello(agent1))),
    Hello2Action = action('.print'(hello(agent2))),
    Hello3Action = action('.print'(hello(agent3))),
    count(Hello1Action,Actions,NumRestartsAgent1),
    count(Hello2Action,Actions,NumRestartsAgent2),
    count(Hello3Action,Actions,NumRestartsAgent3),
    assertion({NumRestartsAgent1,NumRestartsAgent2,NumRestartsAgent3}=={1,2,2}),
    !.

%% Tests that a supervisor uses restart strategies well
test(system_restarts_one_for_one) :-
    outLevel(Level),
    run_system(defsys17_1_one_for_one,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    Hello1Action = action('.print'(hello(agent1))),
    Hello2Action = action('.print'(hello(agent2))),
    Hello3Action = action('.print'(hello(agent3))),
    count(Hello1Action,Actions,NumRestartsAgent1),
    count(Hello2Action,Actions,NumRestartsAgent2),
    count(Hello3Action,Actions,NumRestartsAgent3),
    assertion({NumRestartsAgent1,NumRestartsAgent2,NumRestartsAgent3}=={1,2,1}),
    !.

%% Tests the combination of unblock and restart supervision behaviour.
test(system_restarts_rest_for_all) :-
    outLevel(Level),
    run_system(defsys18,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    RestartAction = action('.print'(restarting)),
    WorkingAction = action('.print'(working)),
    count(RestartAction,Actions,NumRestarts),
    count(WorkingAction,Actions,NumWorkings),
    assertion(NumRestarts==4),
    MinWorkings is NumRestarts*3,
    assertion(NumWorkings>=MinWorkings),
    !.

%% Tests a hierarchical two level supervisor.
test(system_supervisor_hierarchical) :-
    outLevel(Level),
    run_system(defsys19,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    StartAction = action('.print'(agent_started)),
    count(StartAction,Actions,NumStarts),
    LimitNumStarts is (1+3)*(1+2), % 3 = num restarts outer, 2 = num restarts inner
    assertion(NumStarts==LimitNumStarts),
    !.

%% Tests a hierarchical two level supervisor.
test(system_supervisor_time_limits_exceeded) :-
    outLevel(Level),
    run_system(defsys20,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    StartAction = action('.print'(agent_started)),
    count(StartAction,Actions,NumStarts),
    assertion(NumStarts==2),
    !.

%% Tests a hierarchical two level supervisor.
test(system_supervisor_time_limits_not_exceeded) :-
    outLevel(Level),
    run_system(defsys21,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    StartAction = action('.print'(agent_started)),
    count(StartAction,Actions,NumStarts),
    assertion(NumStarts==6),
    !.

%% Test replacing monitors.
test(replacing_monitors) :-
    outLevel(Level),
    run_system(defsys22all,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    Action = action('.print'(hello)),
    assertion(Actions == [Action,Action,Action]),
    !.

%% Test replacing monitors, and checking persistent.
test(non_persistence) :-
    outLevel(Level),
    run_system(defsys22none,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    Action = action('.print'(hello)),
    assertion(Actions == [Action,Action]),
    !.

%% Test monitoring unblocked
test(non_persistence) :-
    outLevel(Level),
    run_system(defsys23,_FinalSystem,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    assertion(Actions == [
                  action('.print'(agent_unblocked(diverging_agent))),
                  action('.print'(agent_up(diverging_agent))),
                  action('.print'(agent_down(diverging_agent)))
              ]),
    !.

test(network1) :-
    outLevel(Level),
    setof(Actions,FinalSys^run_network(defnet1,FinalSys,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(permutation(CollectedActions,[[action('.print'(120))]])),
    !.

test(network2) :-
    outLevel(Level),
    setof(Actions,FinalSys^run_network(defnet2,FinalSys,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(permutation(CollectedActions,[[action('.print'(120))]])),
    !.

test(network3) :-
    outLevel(Level),
    setof(Actions,FinalSys^run_network(defnet3,FinalSys,Actions,[returnActions,outLogLevel(Level)]),CollectedActions),
    assertion(permutation(CollectedActions,[[action('.print'(5))],[action('.print'(0))]])),
    !.

test(network41) :-
    outLevel(Level),
    run_network(defnet41,_,History,[outLogLevel(Level)]),
    actions_history(History,Actions),
    assertion(subset(Actions,[action('.print'(hello)),action('.print'(120))])),
    !.

test(robot_example) :-
    tstRobot(Actions,error), !,
    include(isSip,Actions,Sips),
    length(Sips,LenSips),
    assertion(LenSips =< 11),  %% Exact limit depends on timing
    assertion(member(action('.print'("beer limit exceeded")),Actions)).

test(split_monitor_and_supervise) :-
    outLevel(Level),
    %% Guide the execution so that split occurs when the monitor and the supervisor has been set up
    run_network(defnetwork_split1,_,History,[outLogLevel(Level),guide(guideSp1,notHasSplit(0))]),
    actions_history(History,Actions),
    assertion(permutation(Actions,[action('.print'(ping)),action('.print'(pong)),action('.print'(supervisor))])),
    !.

test(merge_monitor) :-
    outLevel(Level),
    %% Guide the execution so that split occurs when the monitor and the supervisor has been set up
    run_network(defnetwork_merge1,_,History,[terminated(terminated24),outLogLevel(Level),guide(guideMerge,phase1)]),
    actions_history(History,Actions),
    assertion(Actions==[
                  action('.print'(agent_up(diverging_agent))),
                  action('.print'(agent_down(diverging_agent)))
              ]),
    !.

%% test(merge_fail_monitor) :-
%%    outLevel(Level),
%%    %% Guide the execution so that split occurs when the monitor and the supervisor has been set up
%%    run_network(defnetwork_merge2,_,History,[terminated(terminated24),outLogLevel(Level),guide(guideMerge,phase1)]),
%%    actions_history(History,Actions),
%%    assertion(Actions==[
%%                  action('.print'(agent_up(diverging_agent))),
%%                  action('.print'(agent_down(diverging_agent)))
%%              ]),
%%    !.

:- end_tests(jason).

%% count number of elements in a list
count(_,[],0) :-!.
count(E,[E|Rest],N) :- !, count(E,Rest,N1), N is N1+1.
count(E,[_|Rest],N) :- count(E,Rest,N).

        
