%% Agent semantics
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

%% Defines the transition rules for agents in isolation.

%% Agent == agent(Ag,Name,Circumstance,Comm,CycleState,Step,Selectors)
%% Ag == ag(Beliefs,Plans,Init)
%% Init = init(Beliefs,Events)
%% Circumstance == cs(Intentions,Events,Actions)
%% Comm == comm(In,Outs,SI,Perceptions)
%% CycleState = s(RelevantPlans,ApplicablePlans,Intention,Event,ApplicablePlan)
%% Step \in {perceive, procMsg, selEv, relPl, applPl, selAppl, addIM, selInt, execInt, clrInt}
%% Selectors = selectors(EventSelector,...)
%%
%% We consider the initial state of an agent to be its plans, its initial beliefs, and
%% a set of initial events.

agent_step_env(Agent,Rule,AgentNew,Actions,Env,Env) :-
    agent_step(Agent,Rule,AgentNew,Actions,Env).

agent_step(Agent,Rule,AgentNew,Actions,Env) :-
    step(Agent,Step),
    name(Agent,Name),
    message(debug,'agent ~w running step type ~w~n',[Name,Step]),
    ( agent_step(Step,Rule,Agent,AgentNew,Actions,Env) *->
      true 
    ;
    message(error,'agent ~w cannot run step ~w~n',[Name,Step]),
    fail
    ).

agent_step(perceive,perceive,Agent,AgentNew,[],Env) :-
    perceiveAgent(Agent,Agent1,Env),
    setStep(Agent1,procMsg,AgentNew).

agent_step(selEv,Rule,Agent,AgentNew,[],_Env) :-
    eventSelector(Agent,Se),
    events(Agent,Events),
    ( apply(Se,[Events,Event,RestEvents]) *->
        setEvents(Agent,RestEvents,Agent1),
        setStep(Agent1,relPl,Agent2),
        setEvent(Agent2,Event,AgentNew),
	name(Agent,Name),
        ( Event=event(_,empty) ->
	  message(normal,'Agent ~w handling event ~w~n',[Name,Event])
        ;
        ( Event=event(_,[plan(PlanName,_,[Act|_])|_]) ->
          message(normal,'Agent ~w continuing executing plan ~w at ~w~n',[Name,PlanName,Act])
        ;
        true)),
        Rule=selEv1
    ;
        setStep(Agent,selInt,AgentNew),
        Rule=selEv2
    ).

agent_step(relPl,Rule,Agent,AgentNew,[],_Env) :-
    event(Agent,Event),
    plans(Agent,Plans),
    relPlans(Plans,Event,RelevantPlans),
    ( RelevantPlans \== [] ->
        setRelevantPlans(Agent,RelevantPlans,Agent1),
        setStep(Agent1,applPl,AgentNew),
        Rule=rel1
    ;
        name(Agent,Name),
        message(warning,'No relevant plan exists for event ~w in agent ~w~n',[Event,Name]),
        setStep(Agent,selEv,AgentNew),
        Rule=rel2
    ).

agent_step(applPl,Rule,Agent,AgentNew,[],_Env) :-
    relevantPlans(Agent,RelevantPlans),
    beliefs(Agent,Beliefs),
    appPlans(Beliefs,RelevantPlans,ApplicablePlans,Diverges),
    ( Diverges == diverges ->
      Rule=appl3,
      setStep(Agent,diverging,AgentNew) ;
      ( ApplicablePlans \== [] ->
        setApplicablePlans(Agent,ApplicablePlans,Agent1),
        setStep(Agent1,selAppl,AgentNew),
        Rule=appl1
      ;
      name(Agent,Name),
      event(Agent,Event),
      message(warning,'No applicable plan exists in agent ~w~n for event ~w: relevant plans: ~w beliefs: ~w~n~n',[Name,Event,RelevantPlans,Beliefs]),
      setStep(Agent,selInt,AgentNew),
      Rule=appl2
      )
    ).

agent_step(selAppl,selAppl,Agent,AgentNew,[],_Env) :-
    appSelector(Agent,So),
    applicablePlans(Agent,ApplicablePlans),
    apply(So,[ApplicablePlans,{ApplicablePlan,Subst}]),
    message(debug,'ApplicablePlan=~w Subst is ~w~n',[ApplicablePlan,Subst]),
    apply(Subst,[]),
    message(debug,'after substs: plan=~w~n',[ApplicablePlan]),
    setApplicablePlan(Agent,ApplicablePlan,Agent1),
    setStep(Agent1,addIM,AgentNew).

agent_step(addIM,Rule,Agent,AgentNew,[],_Env) :-
    name(Agent,Name),
    event(Agent,Event),
    ( Event=event(HandleEvent,empty) ->
      Rule=extEv,
      applicablePlan(Agent,ApplicablePlan),
      intentions(Agent,Intentions),
      setIntentions(Agent,[[ApplicablePlan]|Intentions],Agent1),
      setStep(Agent1,selInt,AgentNew)
    ;
      Event=event(HandleEvent,EventIntentions),
      Rule=intEv,
      applicablePlan(Agent,ApplicablePlan),
      intentions(Agent,Intentions),
      setIntentions(Agent,[[ApplicablePlan|EventIntentions]|Intentions],Agent1),
      setStep(Agent1,selInt,AgentNew)
    ),
    ApplicablePlan = plan(NP,_,_),
    %% (HandleEvent = add(achieve(goal(Goal))), Rule = extEv ->
       %%  message(normal,'Agent ~w commited to achieving goal ~w through plan ~w~n',[Name,Goal,NP])
    %%;
    %%true).
    (Rule = extEv ->
         true_beliefs(Agent,Beliefs),
         message(normal,'Agent ~w commited to handling event ~w through plan ~w; beliefs: ~w~n',[Name,HandleEvent,NP,Beliefs])
    ;
    true).

agent_step(selInt,Rule,Agent,AgentNew,[],_Env) :-
    intentions(Agent,Intentions),
    ( Intentions \== [] ->
      Rule=selInt1,
      intentionSelector(Agent,Si),
      apply(Si,[Intentions,Intention]),
      setIntention(Agent,Intention,Agent1),
      setStep(Agent1,execInt,AgentNew)
    ;
      Rule = selInt2,
      setStep(Agent,perceive,AgentNew)
    ).

agent_step(execInt,execActSndAsk,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(NP,H,[action(Action)|Body])|RestPart],
    simplify(Action,Action1),
    Action1='.send'(Id,Ilf,Cnt),
    member(Ilf,[askOne,askAll,askHow]),
    !,
    new_mid(Agent,Mid,Agent1),
    Msg = msg(Mid,Id,Ilf,Cnt),
    outs(Agent,Outs),
    setOuts(Agent,[Msg|Outs],Agent2),
    si(Agent1,SI),
    setSI(Agent2,[suspended(Mid,[plan(NP,H,Body)|RestPart])|SI],Agent3),
    intentions(Agent,Intentions),
    delete(Intentions,Intention,Intentions1),
    setIntentions(Agent3,Intentions1,Agent4),
    setStep(Agent4,clrInt,AgentNew).
agent_step(execInt,execActSnd,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(NP,H,[action(Action)|Body])|RestPart],
    simplify(Action,Action1),
    Action1='.send'(Id,Ilf,Cnt),
    \+ member(Ilf,[askOne,askAll,askHow]),
    !,
    new_mid(Agent,Mid,Agent1),
    Msg = msg(Mid,Id,Ilf,Cnt),
    outs(Agent1,Outs),
    setOuts(Agent1,[Msg|Outs],Agent2),
    intentions(Agent2,Intentions),
    NewIntention=[plan(NP,H,Body)|RestPart],
    delete(Intentions,Intention,Intentions1),
    setIntentions(Agent2,[NewIntention|Intentions1],Agent3),
    setStep(Agent3,clrInt,AgentNew).
agent_step(execInt,action,Agent,AgentNew,[action(Action1)],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(NP,H,[action(Action)|Body])|RestPart],
    Action =.. [Pred|_],
    Pred \== '.send',
    !,
    simplify(Action,Action1),
    actions(Agent,Actions),
    setActions(Agent,[action(Action1)|Actions],Agent1),
    NewIntention=[plan(NP,H,Body)|RestPart],
    intentions(Agent,Intentions),
    delete(Intentions,Intention,Intentions1),
    append(Intentions1,[NewIntention],Intentions2), setIntentions(Agent1,Intentions2,Agent2),
    %%setIntentions(Agent1,[NewIntention|Intentions1],Agent2),
    setStep(Agent2,clrInt,AgentNew).
agent_step(execInt,achvGl,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    message(debug,'intention=~w~n',[Intention]),
    Intention=[plan(_,_,[achieve(goal(Goal))|_])|_],
    !,
    message(fine_debug,'before simplify goal=~w~n',[Goal]),
    simplify(Goal,Goal1),
    message(fine_debug,'simplified goal is ~w~n',[Goal1]),
    intentions(Agent,Intentions),
    delete(Intentions,Intention,Intentions1),
    setIntentions(Agent,Intentions1,Agent1),
    addEvents(Agent1,[event(add(achieve(goal(Goal1))),Intention)],Agent2),
    setStep(Agent2,perceive,AgentNew).
agent_step(execInt,Rule,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(NP,Head,[test(TestGoal)|RestPlan])|RestIntention],
    !,
    beliefs(Agent,Beliefs),
    intentions(Agent,Intentions),
    ( test(Beliefs,TestGoal,Diverges) ->
      ( Diverges == diverges ->
        Rule = testGl3,
        setStep(Agent,diverges,AgentNew)
      ;
      Rule=testGl1,
      NewIntention=[plan(NP,Head,RestPlan)|RestIntention],
      delete(Intentions,Intention,Intentions1),
      setIntentions(Agent,[NewIntention|Intentions1],Agent1),
      setStep(Agent1,clrInt,AgentNew))
    ;
          Rule=testGl2,
          delete(Intentions,Intention,Intentions1),
          setIntentions(Agent,Intentions1,Agent1),
          addEvents(Agent1,[event(test(TestGoal),Intention)],Agent2),
          setStep(Agent2,clrInt,AgentNew)
    ).
agent_step(execInt,addBel,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(NP,H,[add(belief(F))|Body])|RestPart],
    !,
    simplify(F,F1),
    AnnBelief = ann(belief(F1),self),
    addBelief(Agent,AnnBelief,Agent1),
    NewIntention=[plan(NP,H,Body)|RestPart],
    setIntention(Agent1,NewIntention,Agent2),
    addEvents(Agent2,[event(add(AnnBelief),empty)],Agent3),
    intentions(Agent3,Intentions),
    delete(Intentions,Intention,Intentions1),
    setIntentions(Agent3,[NewIntention|Intentions1],Agent4),
    setStep(Agent4,clrInt,AgentNew).
agent_step(execInt,delBel,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(NP,H,[del(belief(F))|Body])|RestPart],
    !,
    simplify(F,F1),
    AnnBelief = ann(belief(F1),self),
    delBelief(Agent,AnnBelief,Agent1),
    NewIntention=[plan(NP,H,Body)|RestPart],
    setIntention(Agent1,NewIntention,Agent2),
    addEvents(Agent2,[event(del(AnnBelief),empty)],Agent3),
    intentions(Agent3,Intentions),
    delete(Intentions,Intention,Intentions1),
    setIntentions(Agent3,[NewIntention|Intentions1],Agent4),
    setStep(Agent4,clrInt,AgentNew).
agent_step(execInt,emptyBody,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(_,_,[])|_],
    !,
    setStep(Agent,clrInt,AgentNew).
agent_step(execInt,Rule,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention=[plan(NP,H,[predicate(Pred)|Body])|RestPart],
    !,
    beliefs(Agent,Beliefs),
    solve(Pred,Beliefs,Diverges),
    ( Diverges == diverges *->
      Rule = predicate_diverges,
      setStep(Agent,diverges,AgentNew)
    ;
    Rule = predicate,
    NewIntention=[plan(NP,H,Body)|RestPart],
    setIntention(Agent,NewIntention,Agent1),
    intentions(Agent1,Intentions),
    delete(Intentions,Intention,Intentions1),
    setIntentions(Agent1,[NewIntention|Intentions1],Agent2),
    setStep(Agent2,clrInt,AgentNew)).

agent_step(clrInt,clrInt1,Agent,AgentNew,[],_Env) :-
    name(Agent,Name),
    intention(Agent,Intention),
    Intention = [plan(NP,Trigger,[])],
    !,
    (Trigger = trigger(add(achieve(goal(TriggerGoal))),_) ->
	 message(normal,'Agent ~w achieved goal ~w through plan ~w~n',[Name,TriggerGoal,NP])
    ;
    true),
    intentions(Agent,Intentions),
    delete(Intentions,Intention,Intentions1),
    setIntentions(Agent,Intentions1,Agent1),
    setStep(Agent1,perceive,AgentNew).
agent_step(clrInt,clrInt2,Agent,AgentNew,[],_Env) :-
    name(Agent,Name),
    intention(Agent,Intention),
    Intention = [plan(NP,Trigger,[])|Rest],
    !,
    (Trigger = trigger(add(achieve(goal(TriggerGoal))),_) ->
	 message(normal,'Agent ~w achieved goal ~w through plan ~w~n',[Name,TriggerGoal,NP])
    ;
    true),
    Rest \== [],
    Rest = [plan(NP2,H,[_|RestPlan])|RestPlans],
    intentions(Agent,Intentions),
    delete(Intentions,Intention,Intentions1),
    NewIntention=[plan(NP2,H,RestPlan)|RestPlans],
    NewIntentions=[NewIntention|Intentions1],
    setIntention(Agent,NewIntention,Agent1),
    setIntentions(Agent1,NewIntentions,Agent2),
    setStep(Agent2,clrInt,AgentNew).
agent_step(clrInt,clrInt3,Agent,AgentNew,[],_Env) :-
    intention(Agent,Intention),
    Intention \== [plan(_,_,[])|_],
    setStep(Agent,perceive,AgentNew).

agent_step(procMsg,Rule,Agent,NewAgent,[],_Env) :-
    name(Agent,Name),
    ins(Agent,Ins),
    ( Ins \== [] ->
      msg(Agent,Msg,Type,IsReply),
      receive_message({Type,IsReply},Msg,Rule,Agent,NewAgent),
      message(normal,'Agent ~w received message ~w (isReply ~w)~n',[Name,Msg,IsReply])
    ;
    Rule=noMsg,
    setStep(Agent,selEv,NewAgent)
    ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

perceiveAgent(Agent,AgentNew,Env) :-
    name(Agent,Name),
    perceptions(Agent,OldPerceptions),
    Env = env(State,PP),
    ( apply(PP,[State,NewPerceptions]) *->
      include(agentPerception(Name),NewPerceptions,PreAgentPerceptions),
      maplist(stripName,PreAgentPerceptions,AgentPerceptions),
      ( OldPerceptions == none ->
	initializeBeliefs(Agent,AgentPerceptions,Agent1)
      ;
      updateBeliefs(Agent,OldPerceptions,AgentPerceptions,Agent1)
      ),
      setPerceptions(Agent1,AgentPerceptions,AgentNew)
    ;
    message(error,'Perception predicate ~w fails when applied to environment state ~w~n',[PP,State]),
    abort
    ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

receive_message({tell,noreply},Msg,tell,Agent,NewAgent) :-
    Msg = msg(_Mid,Id,_Type,Belief),
    ins(Agent,Ins),
    delete(Ins,Msg,Ins1),
    setIns(Agent,Ins1,Agent1),
    annotateBelief(Id,Belief,AnnotatedBelief),
    makeEmptyAddEvent(AnnotatedBelief,BeliefEvent),
    addEvents(Agent1,[BeliefEvent],Agent2),
    beliefs(Agent2,AgBeliefs),
    setBeliefs(Agent2,[AnnotatedBelief|AgBeliefs],Agent3),
    setStep(Agent3,selEv,NewAgent).

receive_message({tell,reply},Msg,tellRepl,Agent,NewAgent) :-
    Msg = msg(Mid,Id,_Type,Belief),
    ins(Agent,Ins),
    delete(Ins,Msg,Ins1),
    setIns(Agent,Ins1,Agent1),
    si(Agent1,SI),
    member(Item,SI),
    Item = suspended(Mid,Intention),
    delete(SI,Item,SI1),
    setSI(Agent1,SI1,Agent2),
    intentions(Agent2,Intentions),
    setIntentions(Agent2,[Intention|Intentions],Agent3),
    annotateBelief(Id,Belief,AnnotatedBelief),
    makeEmptyAddEvent(AnnotatedBelief,BeliefEvent),
    addEvents(Agent3,[BeliefEvent],Agent4),
    beliefs(Agent4,Beliefs),
    setBeliefs(Agent4,[AnnotatedBelief|Beliefs],Agent5),
    setStep(Agent5,selEv,NewAgent).

receive_message({unTell,reply},Msg,untellRepl,Agent,NewAgent) :-
    Msg = msg(Mid,Id,_Type,Belief),
    ins(Agent,Ins),
    delete(Ins,Msg,Ins1),
    setIns(Agent,Ins1,Agent1),
    si(Agent1,SI),
    member(Item,SI),
    Item = suspended(Mid,Intention),
    delete(SI,Item,SI1),
    setSI(Agent1,SI1,Agent2),
    intentions(Agent2,Intentions),
    setIntentions(Agent2,[Intention|Intentions],Agent3),
    annotateBelief(Id,Belief,AnnotatedBelief),
    makeEmptyDelEvent(AnnotatedBelief,BeliefEvent),
    addEvents(Agent3,[BeliefEvent],Agent4),
    beliefs(Agent4,Beliefs),
    delete(Beliefs,BeliefEvent,Beliefs1),
    setBeliefs(Agent4,Beliefs1,Agent5),
    setStep(Agent5,selEv,NewAgent).

receive_message({askOne,noreply},Msg,Rule,Agent,NewAgent) :-
    Msg = msg(Mid,Id,_Type,B),
    ins(Agent,Ins),
    delete(Ins,Msg,Ins1),
    setIns(Agent,Ins1,Agent1),
    beliefs(Agent1,Beliefs),
    outs(Agent1,Outs),
    ( test(Beliefs,B,Diverges) ->
      ( Diverges == diverges ->
        setStep(Agent,askOne_diverges,NewAgent)
      ; 
      setOuts(Agent1,[msg(Mid,Id,tell,B)|Outs],Agent2)
      )
    ;
    setOuts(Agent1,[msg(Mid,Id,unTell,B)|Outs],Agent2)
    ),
    Rule = askOne,
    setStep(Agent2,selEv,NewAgent).

receive_message({achieve,noreply},Msg,achieve,Agent,NewAgent) :-
    Msg = msg(_Mid,_Id,_Type,At),
    ins(Agent,Ins),
    delete(Ins,Msg,Ins1),
    setIns(Agent,Ins1,Agent1),
    addEvents(Agent1,[event(add(achieve(At)),empty)],Agent2),
    setStep(Agent2,selEv,NewAgent).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

agent_terminated(Agent,_) :-
    step(Agent,diverges), !.
agent_terminated(Agent,_) :-
    step(Agent,stopping), !.
agent_terminated(Agent,_) :-
    step(Agent,diverged), !.
agent_terminated(Agent,_) :-
    step(Agent,stopped), !.

agent_terminated(Agent,Env) :-
    events(Agent,[]),
    intentions(Agent,[]),
    ins(Agent,[]),
    perceiveAgent(Agent,Agent,Env),
    step(Agent,selEv).

agent_printer(Stream,Agent) :-
    name(Agent,Name),
    actions(Agent,Actions),    
    beliefs(Agent,Beliefs),
    events(Agent,Events),
    intentions(Agent,Intentions),
    intention(Agent,Intention),
    step(Agent,Step),
    ins(Agent,Ins),
    outs(Agent,Outs),
    si(Agent,SI),
    perceptions(Agent,Perceptions),
    format(Stream,'agent ~w: {~n  step=~w~n',[Name,Step]),
    (Events \== [] -> format(Stream,'  events=~w~n',[Events]); true),
    (Actions \== [] -> format(Stream,'  actions=~w~n',[Actions]); true),
    (Beliefs \== [] -> format(Stream,'  beliefs=~w~n',[Beliefs]); true),
    (Intentions \== [] -> format(Stream,'  intentions=~w~n',[Intentions]); true),
    (Ins \== [] -> format(Stream,'  ins=~w~n',[Ins]); true),
    (Outs \== [] -> format(Stream,'  outs=~w~n',[Outs]); true),
    (SI \== [] -> format(Stream,'  si=~w~n',[SI]); true),
    (Perceptions \== [] -> format(Stream,'  perceptions=~w~n',[SI]); true),
    (var(Intention) -> true ; format(Stream,'  intention=~w~n}~n',[Intention])).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

consequence(PlanFact,EventFact,PlanFactR=EventFactR) :-
    message(debug,'consequence planfact ~w eventfact ~w~n',[PlanFact,EventFact]),
    annotatedFact(PlanFact,PlanFactR,Ann1),
    annotatedFact(EventFact,EventFactR,Ann2),
    copy_term(EventFactR,ER),
    PlanFactR=ER,
    subset(Ann1,Ann2).

consequence_np(PlanFact,EventFact,PlanFactR=EventFactR) :-
    annotatedFact(PlanFact,PlanFactR,Ann1),
    annotatedFact(EventFact,EventFactR,Ann2),
    message(debug,'consequence planfact ~w eventfact ~w Ann1=~w Ann2=~w~n',[PlanFactR,EventFactR,Ann1,Ann2]),
    PlanFactR=EventFactR,
    subset(Ann1,Ann2).

annotatedFact(X,X,[]) :- var(X), !.
annotatedFact(ann(F,Ann),R,[Ann|AnnL]) :-
    !,
    annotatedFact(F,R,AnnL).
annotatedFact(F,F,[]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

relPlans(Plans,event(Event,_),RelevantPlans) :-
    maplist(copy_term,Plans,Plans1),
    includeResults(relPlan(Event),Plans1,RelevantPlans),
    message(debug,'event=~w~n  possible plans=~w~n  relevant plans=~w~n',[Event,Plans1,RelevantPlans]),
    true.

relPlan(Event,Plan,{Plan,SubstPred}) :-
    message(fine_debug,'Checking plan: event=~w plan=~w~n',[Event,Plan]),
    trEv(Plan,PlanTE),
    trSep(Event,Type,EventF),
    trSep(PlanTE,Type,PlanF),
    consequence(PlanF,EventF,SubstPred).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

appPlans(Beliefs,Plans,ApplicablePlans,Diverges) :-
    appPlans1(Plans,Beliefs,ApplicablePlans,Diverges),
    message(debug,'~nrelevant plans=~w~n  applicable plans=~w~n',[Plans,ApplicablePlans]),
    true.

appPlans1([],_Beliefs,[],non_divergent).
appPlans1([{Plan,Subst}|RestPlans],Beliefs,AppPlans,Diverges) :-
    copy_term({Plan,Subst},{Plan1,Subst1}),
    ( appPlan(Beliefs,Plan1,Subst1,Diverges1) ->
      ( Diverges1 == diverges ->
        Diverges = Diverges1
      ;
      NewSubst = appPlan(Beliefs,Plan,Subst,_),
      AppPlans=[{Plan,NewSubst}|RestAppPlans],
      appPlans1(RestPlans,Beliefs,RestAppPlans,Diverges)
      )
    ;
    appPlans1(RestPlans,Beliefs,AppPlans,Diverges)
    ).

appPlan(Beliefs,Plan,Subst,Diverges) :-
    message(fine_debug,'Checking plan ~w with subst ~w~n',[Plan,Subst]),
    apply(Subst,[]),
    ctxt(Plan,Ct),
    solve(Ct,Beliefs,Diverges).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

test(Beliefs,TestGoal,Diverges) :-
    solve(TestGoal,Beliefs,Diverges).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

includeResults(_P,[],[]).
includeResults(P,[First|Rest],Result) :-
    includeResults(P,Rest,RestResults),
    ( apply(P,[First,FirstResult]) ->
         Result = [FirstResult|RestResults]
    ;
         message(fine_debug,'failed, continuing~n',[]),
         Result = RestResults
    ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

trEv(plan(_,trigger(Te,_Ct),_H),Te).
ctxt(plan(_,trigger(_Te,Ct),_H),Ct).
trSep(add(ann(X,_)),Y,Z) :-
    trSep(add(X),Y,Z).
trSep(del(ann(X,_)),Y,Z) :-
    trSep(del(X),Y,Z).
trSep(add(belief(F)),add(belief(_)),F).
trSep(del(belief(F)),del(belief(_)),F).
trSep(add(achieve(goal(F))),add(achieve(goal(_))),F).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Always select oldest event first
stdEvSel([First|Rest],First,Rest).
stdAppSel(L,X) :-
    randomized_member(X,L).
stdIntentionSel(L,X) :-
    randomized_member(X,L).
stdSociallyAcceptable(_,_,_).
stdSmSelector(L,X) :-
    member(X,L).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% A jason (prolog) goal solver which can decide
%% that a computation diverges if too many computation
%% steps are executed.

solve(Goal,Beliefs,Diverges) :-
    solve(Goal,Beliefs,0,N),
    ( N > 100 -> Diverges = diverges
    ;
    Diverges = no_divergence
    ).

%% Divergence check
solve(_,_,N,N) :-
    N > 100,
    !.
solve(true, _, N, N1) :-
    !,
    N1 is N+1.
solve('.date'(YY,MM,DD),_,N,N1) :-
    !,
    N1 is N+1,
    get_time(Time), stamp_date_time(Time,D,'UTC'),
    date_time_value(year,D,YY),
    date_time_value(month,D,MM),
    date_time_value(day,D,DD).
solve('.time'(HH,NN,SS),_,N,N1) :-
    !,
    N1 is N+1,
    get_time(Time), stamp_date_time(Time,D,'UTC'),
    date_time_value(hour,D,HH),
    date_time_value(minute,D,NN),
    date_time_value(second,D,SS).
solve('.count'(Goal,Var),Beliefs,N,N1) :-
    !,
    findall(Steps,solve(Goal,Beliefs,0,Steps),StepsList),
    sum_list(StepsList,GoalSteps),
    N1 is N+GoalSteps,
    length(StepsList,Var).
solve((A,B), Beliefs, N, N2) :-
    !,
    solve(A, Beliefs, N, N1),
    solve(B, Beliefs, N1, N2).
solve(A=B, _Beliefs, N, N1) :-
    simplify(A,A1),
    simplify(B,B1),
    !,
    N1 is N+1,
    A1=B1.
solve(not(Goal),Beliefs,N,N1) :-
    %% Bad approximation
    ( solve(Goal,Beliefs,N,N1) -> fail; N1 is N+1 ).
solve('<'(X,Y), _,N,N1) :-
    !,
    N1 is N+1,
    ground(X),
    ground(Y),
    X<Y.
solve('>'(X,Y), _,N,N1) :-
    !,
    N1 is N+1,
    ground(X),
    ground(Y),
    X>Y.
solve('>='(X,Y), _,N,N1) :-
    !,
    N1 is N+1,
    ground(X),
    ground(Y),
    X>=Y.
solve('=='(X,Y), _,N,N1) :-
    !,
    N1 is N+1,
    ground(X),
    ground(Y),
    X==Y.
solve(belief(Belief),Beliefs,N,N1) :-
    !,
    N1 is N+1,
    member(OtherBelief,Beliefs),
    is_belief(OtherBelief),
    consequence_np(belief(Belief),OtherBelief,_).
solve(Goal, Beliefs,N,N1) :-
    %% we should probably simplify arguments of the goal
    N2 is N+1,
    message(fine_debug,'solve(~w) beliefs: ~w~n',[Goal,Beliefs]),
    pclause(Goal, Body, Beliefs),
    message(fine_debug,'solve(~w) continuing with ~w~n',[Goal,Body]),
    solve(Body, Beliefs, N2, N1).

simplify(V,V) :-
    var(V), !.
simplify(ann(F,Ann),ann(R,Ann)) :-
    simplify(F,R).
simplify([],[]) :- !.
simplify([T|Rest1],[R|Rest2]) :- 
    !,
    simplify(T,R),
    simplify(Rest1,Rest2).
simplify('*'(X,Y),Result) :-
    number(X), number(Y),
    !,
    simplify(X,XR),
    simplify(Y,YR),
    Result is XR*YR.
simplify('+'(X,Y),Result) :-
    number(X), number(Y),
    !,
    simplify(X,XR),
    simplify(Y,YR),
    Result is XR+YR.
simplify('-'(X,Y),Result) :-
    number(X), number(Y),
    !,
    simplify(X,XR),
    simplify(Y,YR),
    Result is XR-YR.
simplify(P1,P2) :-
    P1 =.. [Pred|Args1],
    Args1 \== [],
    !,
    simplify(Args1,Args2),
    P2 =.. [Pred|Args2].
simplify(Other,Other).
    
pclause(Goal, Body, [Def|_]) :-
    copy_term(Def,Def1),
    Def1 = (Goal :- Body).
pclause(Goal, Body, [Def|_]) :-
    \+ functor(Def,(:-),2),
    copy_term(Def,Def1),
    Def1 = Goal,
    Body = true.
pclause(Goal, Body, [_|Rest]) :-
    pclause(Goal, Body, Rest).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

msg(Agent,Msg,Type,IsReply) :-
    ins(Agent,Ins),
    Ins \== [],
    smSelector(Agent,SM),
    apply(SM,[Ins,Msg]),
    Msg = msg(Mid,Id,Type,Content),
    sociallyAcceptableSelector(Agent,SociallyAcceptable),
    apply(SociallyAcceptable,[Id,Type,Content]),
    si(Agent,SI),
    ( member(suspended(Mid,_),SI) ->
      IsReply=reply
    ;
    IsReply=noreply
    ).

annotateBelief(Id,belief(Belief),ann(belief(Belief),Id)).
makeEmptyAddEvent(F,event(add(F),empty)).
makeEmptyDelEvent(F,event(del(F),empty)).
    
agentPerception(Name,{Name,_}).
stripName({_,Percept},Percept).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

agentParams(Params) :-
    stdParams(Std),
    setTransRel(Std,agent_step_env,agent_terminated,agent_printer,Params).

run_agent(AgentPred,History) :-
    run_agent(AgentPred,History,[]).

run_agent(AgentPred,History,ExtraParams) :-
    apply(AgentPred,[Agent]),
    agentParams(PreParams),
    append(ExtraParams,PreParams,Params),
    runTerminate(Agent,_,History,Params,_).
