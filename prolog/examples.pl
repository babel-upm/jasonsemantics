%% Examples
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)
%%
%% A set of examples used for testing the Jason implementation.
%% Test code is located in tests.pl.
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%% Agent examples

defag1(Agent) :-
    Plan1 = plan(plan1,trigger(add(belief(fact(X,Y))),'<'(X,5)),[add(belief(fact('+'(X,1),'*'('+'(X,1),Y))))]),
    Plan2 = plan(plan2,trigger(add(belief(fact(X,Y))),'=='(X,5)),[action('.print'(Y))]),
    Init = init([],[event(add(belief(fact(0,1))),empty)]),
    create_agent(n1,[Plan1,Plan2],Init,Agent).  

defag2(Agent) :-
    Plan1 = plan(plan1,trigger(add(achieve(goal(print_fact(N)))),true),[achieve(goal(fact(N,F))),action('.print'(F))]),
    Plan2 = plan(plan2,trigger(add(achieve(goal(fact(N,1)))),'=='(N,0)),[]),
    Plan3 = plan(plan3,trigger(add(achieve(goal(fact(N,F)))),'>'(N,0)),[achieve(goal(fact('-'(N,1),F1))),predicate(F = '*'(F1,N))]),
    Init = init([],[event(add(achieve(goal(print_fact(5)))),empty)]),
    create_agent(n2,[Plan1,Plan2,Plan3],Init,Agent).

defag3(Agent) :-
    Plan1 = plan(plan1,trigger(add(achieve(goal(print(X)))),true),[action('.print'(X))]),
    Plan2 = plan(plan2,trigger(add(achieve(goal(print(X)))),true),[action('.print'(0))]),
    Init = init([],[event(add(achieve(goal(print(5)))),empty)]),
    create_agent(n2,[Plan1,Plan2],Init,Agent).

defag14(Agent) :-
    Init = init([
                    is_a_fine_goal :- is_a_fine_goal
                ],
                [
                    event(add(achieve(goal(my_goal))),empty)
                ]),
    Plan1 = plan(plan1,trigger(add(achieve(goal(my_goal))),true),
                 [
                     action('.print'(fine)),
                     predicate(is_a_fine_goal)
                 ]),
    create_agent(n,[Plan1],Init,Agent).

defag15(Agent) :-
    create_agent(monitor,
     [
         plan(start_monitoring,trigger(add(achieve(goal(start_monitoring))),true),
              [
                  action('.monitor'('ping',[]))
                  ,action('.monitor'('pong',[]))
                  ,action('.monitor'('supervisor',[]))
              ])
     ],
     init([],[event(add(achieve(goal(start_monitoring))),empty)]),Agent).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%% System examples

fail(_) :- fail.


defsys1(System) :-
    defag1(Agent),
    System = sys([{runtime1,[Agent]}], [], [], [], 0).

defsys2(System) :-
    defag2(Agent),
    System = sys([{runtime1,[Agent]}], [], [], [], 0).

defsys3(System) :-
    defag3(Agent),
    System = sys([{runtime3,[Agent]}], [], [], [], 0).

defsys4(System) :-
    Plan1 = plan(plan1,trigger(add(achieve(goal(do_print_hello))),true),[action('.print'('hello'))]),
    Plan2 = plan(plan2,trigger(add(achieve(goal(create_an_agent))),true),
                 [
                  action('.monitor'('hello_agent',[agent_up,agent_down,agent_unblocked]))
                  ,action('.create_agent'('hello_agent',[Plan1],init([],[])))
                 ]),
    Plan3 = plan(plan3,trigger(add(belief(agent_up('hello_agent'))),true),
                 [
                     action('.send'('hello_agent',achieve,goal(do_print_hello)))
                 ]),
    Init = init([],[event(add(achieve(goal(create_an_agent))),empty)]),
    create_agent(n,[Plan2,Plan3],Init,Agent),
    System = sys([{runtime4,[Agent]}], [], [], [], 0).

defsys5(System) :-
    Plan1 = plan(plan1,trigger(add(achieve(goal(kill))),true),[action('.kill_agent'(printer))]),
    Plan2 = plan(plan2,trigger(add(achieve(goal(do_print_hello))),true),[
                     action('.print'('hello')),
                     action('.send'(mean,achieve,goal(kill)))
                 ]),
    Plan3a = plan(plan3a,trigger(add(belief(agent_up('printer'))),true),
                 [
                     action('.send'('printer',achieve,goal(do_print_hello)))
                 ]),
    Plan3b = plan(plan3b,trigger(add(achieve(goal(do_monitor))),true),
                  [
                      action('.monitor'('printer',[agent_up,agent_down,agent_unblocked]))
                  ]),
    RestartPolicy = limits(1,1000),
    Strategy = rest_for_all,
    Plan4 = plan(plan4,trigger(add(achieve(goal(startup))),true),
                 [
                     action('.create_agent'(monitor,[Plan3a,Plan3b],init([],[event(add(achieve(goal(do_monitor))),empty)]))),
                     action('.supervise'([
                                                {runtime4,printer,[Plan2],init([],[])}
                                            ],
                                         sup_policy(_UnblockPolicy,RestartPolicy,Strategy)))
                 ]),
    create_agent(supervisor,[Plan4],init([],[event(add(achieve(goal(startup))),empty)]),Supervisor),
    create_agent(mean,[Plan1],init([],[]),Mean),
    System = sys([{runtime4,[Supervisor,Mean]}], [], [], [], 0).

defsys6(System) :-
    create_agent(agent1,[],init([belief(world_round)],[]),Agent1),
    Plan0 = plan(plan0,trigger(add(belief(world_round)),true),[action('.print'(the_world_is_round))]),
    Plan1 = plan(plan1,trigger(add(belief(world_flat)),true),[action('.print'(the_world_is_flat))]),
    Plan2 = plan(plan2,trigger(add(achieve(goal(knowledge))),true),
                 [
                     action('.send'(agent1,askOne,belief(world_round))),
                     action('.send'(agent1,askOne,belief(world_flat)))
                 ]),
    create_agent(agent2,[Plan0,Plan1,Plan2],init([],[event(add(achieve(goal(knowledge))),empty)]),Agent2),
    System = sys([{runtime,[Agent1,Agent2]}], [], [], [], 0).

defsys7(System) :-
    Plan1 = plan(plan1,trigger(add(achieve(goal(do_print_hello))),true),[action('.print'('hello'))]),
    Plan2 = plan(plan2,trigger(add(achieve(goal(create_an_agent))),true),
                 [
                  action('.create_agent'('hello_agent',[Plan1],init([],[])))
                  ,action('.send'('hello_agent',achieve,goal(do_print_hello)))
                  %%,action('.send'('hello_agent',tell,belief(interesting),belief(fact)))
                 ]),
    Init = init([],[event(add(achieve(goal(create_an_agent))),empty)]),
    create_agent(n,[Plan2],Init,Agent),
    System = sys([{runtime2,[Agent]}], [], [], [], 0).

defsys14(System) :-
    defag14(Agent),
    System = sys([{runtime4,[Agent]}], [], [], [], 0).

defsys14_sup(System) :-
    UnblockPolicy = limits(1,1000),
    RestartPolicy = limits(1,1000),
    Strategy = rest_for_all,
    InitN = init([
                    is_a_fine_goal :- is_a_fine_goal
                ],
                [
                    event(add(achieve(goal(my_goal))),empty)
                ]),
    Plan1 = plan(plan1,trigger(add(achieve(goal(my_goal))),true),
                 [
                     action('.print'(fine)),
                     predicate(is_a_fine_goal)
                 ]),
    Plan4 = plan(plan4,trigger(add(achieve(goal(supervise))),true),
                 [
                     action('.supervise'([{runtime4,n,[Plan1],InitN}],
                                         sup_policy(UnblockPolicy,RestartPolicy,Strategy)))
                 ]),
    create_agent(supervisor,[Plan4],init([],[event(add(achieve(goal(supervise))),empty)]),Supervisor),
    PlanSender = plan(plan_sender,trigger(add(achieve(goal(do_send))),true),
                      [
                          action('.send'(n,achieve,goal(my_goal))),
                          action('.send'(n,achieve,goal(my_goal)))
                      ]),
    create_agent(sender,[PlanSender],init([],[event(add(achieve(goal(do_send))),empty)]),Sender),
    System = sys([{runtime4,[Supervisor,Sender]}], [], [], [], 0).

defsys15(Sys) :-
    defag15(Agent),
    Sys = sys([{runtime15,[Agent]}],[],[],[],0).
    

%% Check that an agent is killed and restarted three times.
defsys16(System) :-
    PlanK = plan(plan1,trigger(add(achieve(goal(kill))),true),[action('.kill_agent'(server))]),
    PlanH = plan(plan2,trigger(add(achieve(goal(do_print_hello))),true),
                 [
                  action('.print'('hello')),
                  action('.send'('stopper',achieve,goal(kill)))
                 ]),
    RestartPolicy = limits(3,1000),
    Strategy = rest_for_all,
    PlanS = plan(plan4,trigger(add(achieve(goal(startup))),true),
                 [
                     action('.supervise'([{runtime4,server,[PlanH],init([],[event(add(achieve(goal(do_print_hello))),empty)])}],
                                         sup_policy(_UnblockPolicy,RestartPolicy,Strategy)))
                 ]),
    create_agent(supervisor,[PlanS],init([],[event(add(achieve(goal(startup))),empty)]),Supervisor),
    create_agent(stopper,[PlanK],init([],[]),Stopper),
    System = sys([{runtime4,[Supervisor,Stopper]}], [], [], [], 0).

defsys17(System,KillAgent,Strategy) :-
    AllStartedPred = (all_started :- ('.count'(belief(started(_)),NumStarted),NumStarted >= 3)),
    PlanK = plan('K',trigger(add(belief(started(_))),(all_started,belief(should_kill(AgentName)))),
		 [
                     action('.kill_agent'(AgentName))
                 ]),
    PlanH = plan('H',trigger(add(achieve(goal(report_name(AgentName)))),true),
                 [
                     action('.print'('hello'(AgentName)))
		     ,action('.send'('stopper',tell,belief(started(AgentName))))
                 ]),
    RestartPolicy = limits(1,1000),
    PlanS = plan('S',trigger(add(achieve(goal(startup))),true),
                 [
                     action('.create_agent'('stopper',[PlanK],init([AllStartedPred,belief(should_kill(KillAgent))],[]))),
                     action('.supervise'(
				[
				    {runtime4,agent1,[PlanH],init([],[event(add(achieve(goal(report_name(agent1)))),empty)])}
				    ,{runtime4,agent2,[PlanH],init([],[event(add(achieve(goal(report_name(agent2)))),empty)])}
				    ,{runtime4,agent3,[PlanH],init([],[event(add(achieve(goal(report_name(agent3)))),empty)])}
				],
                                sup_policy(_UnblockPolicy,RestartPolicy,Strategy)))
                 ]),
    create_agent(supervisor,[PlanS],init([],[event(add(achieve(goal(startup))),empty)]),Supervisor),
    %%create_agent(stopper,[PlanK1,PlanK2],init([belief(should_kill(KillAgent))],[]),Stopper),
    System = sys([{runtime4,[Supervisor]}], [], [], [], 0).
defsys17_1_rest_for_all(System) :- defsys17(System,agent2,rest_for_all).
defsys17_1_rest_for_one(System) :- defsys17(System,agent2,rest_for_one).
defsys17_1_one_for_one(System) :- defsys17(System,agent2,one_for_one).

defsys18(System) :-
    InitN = init([
                    is_a_fine_goal :- is_a_fine_goal
                ],
                [
                    event(add(achieve(goal(start))),empty)
                ]),
    Plan1 = plan(plan1,trigger(add(achieve(goal(start))),true),
                 [
                     action('.print'(restarting)),
                     action('.send'(n,achieve,goal(status_goal)))
                 ]),
    Plan2 = plan(plan2,trigger(add(achieve(goal(status_goal))),true),
                 [
                     action('.print'(working)),
                     action('.send'(n,achieve,goal(status_goal))),
                     predicate(is_a_fine_goal)
                 ]),
    Plan4 = plan(plan4,trigger(add(achieve(goal(supervise))),true),
                 [
                     action('.supervise'([{runtime4,n,[Plan1,Plan2],InitN}],
                                         sup_policy(UnblockPolicy,RestartPolicy,Strategy)))
                 ]),
    Strategy = one_for_one,
    UnblockPolicy = limits(2,1000),
    RestartPolicy = limits(3,1000),
    create_agent(supervisor,[Plan4],init([],[event(add(achieve(goal(supervise))),empty)]),Supervisor),
    System = sys([{runtime4,[Supervisor]}], [], [], [], 0).

%% Test that a hierarchical, two-level, supervisor works.
defsys19(System) :-
    %% Kill agent
    PlanK = plan(planK,trigger(add(achieve(goal(kill_ag))),true),[action('.kill_agent'(ag))]),

    PlanAgent =  plan(planAgent,trigger(add(achieve(goal(start))),true),
                      [
                          action('.print'(agent_started)),
                          action('.send'('kill_agent',achieve,goal(kill_ag)))
                      ]),

    %% Outer server
    PlanOuterSupervisor = plan(planOuterSupervisor,trigger(add(achieve(goal(start))),true),
                 [
                     action('.create_agent'('kill_agent',[PlanK],init([],[]))),
                     action('.supervise'([{runtime4,innerSupervisor,[PlanInnerSupervisor],init([],[event(add(achieve(goal(start))),empty)])}],
                                         sup_policy(limits(1,1000),limits(3,1000),one_for_one)))
                 ]),

    PlanInnerSupervisor = plan(planInnerSupervisor,trigger(add(achieve(goal(start))),true),
                 [
                     action('.supervise'([{runtime4,ag,[PlanAgent],init([],[event(add(achieve(goal(start))),empty)])}],
                                         sup_policy(limits(1,1000),limits(2,1000),one_for_one)))
                 ]),
    
    create_agent(outerSupervisor,[PlanOuterSupervisor],init([],[event(add(achieve(goal(start))),empty)]),OuterSupervisor),
    System = sys([{runtime4,[OuterSupervisor]}], [], [], [], 0).

%% Test that timing computation works for supervisors
defsys20(System) :-
    %% Kill agent
    PlanK = plan(planK,trigger(add(achieve(goal(kill_ag))),true),[action('.kill_agent'(ag))]),

    PlanAgent =  plan(planAgent,trigger(add(achieve(goal(start))),true),
                      [
                          action('.print'(agent_started)),
                          action('.send'('kill_agent',achieve,goal(kill_ag)))
                      ]),
    PlanSupervisor = plan(planSupervisor,trigger(add(achieve(goal(start))),true),
                 [
                     action('.create_agent'('kill_agent',[PlanK],init([],[]))),
                     action('.supervise'([{runtime4,ag,[PlanAgent],init([],[event(add(achieve(goal(start))),empty)])}],
                                         sup_policy(limits(0,1000),limits(1,1000),one_for_one)))
                 ]),
    
    create_agent(supervisor,[PlanSupervisor],init([],[event(add(achieve(goal(start))),empty)]),Supervisor),
    System = sys([{runtime4,[Supervisor]}], [], [], [], 0).

%% Test that timing computation works for supervisors
defsys21(System) :-
    %% Kill agent
    TooManyKilledPred = (too_many_killed :- ('.count'(belief(kill_ag(_)),NumKilled),NumKilled >= 5)),
    PlanK = plan(planK,trigger(add(achieve(goal(kill_ag(Time)))),not(too_many_killed)),[
		     add(belief(kill_ag(Time))),
		     action('.kill_agent'(ag))
		 ]),

    PlanAgent =  plan(planAgent,trigger(add(achieve(goal(start))),true),
                      [
                          action('.print'(agent_started)),
			  action('.inc_time'(1400)),
			  predicate('.time'(HH,NN,SS)),
                          action('.send'('kill_agent',achieve,goal(kill_ag({HH,NN,SS}))))
                      ]),
    PlanSupervisor = plan(planSupervisor,trigger(add(achieve(goal(start))),true),
                 [
                     action('.create_agent'('kill_agent',[PlanK],init([TooManyKilledPred],[]))),
                     action('.supervise'([{runtime4,ag,[PlanAgent],init([],[event(add(achieve(goal(start))),empty)])}],
                                         sup_policy(limits(0,1000),limits(1,1200),one_for_one)))
                 ]),
    
    create_agent(supervisor,[PlanSupervisor],init([],[event(add(achieve(goal(start))),empty)]),Supervisor),
    System = sys([{runtime4,[Supervisor]}], [], [], [], 0).


%% Monitor twice, from the same agent, another agent that is killed, and should be restarted.
defsys22(System,Persistent) :-
    PlanMonitored = plan(planMonitored,trigger(add(achieve(goal(do_print_hello))),true),[
                             action('.print'('hello')),
                             action('.send'(meanAgent,achieve,goal(kill)))
                         ]),
    MeanPlan1 = plan(meanPlan,trigger(add(achieve(goal(kill))),(not(belief(once_killed)),not(belief(twice_killed)))),[
                        add(belief(once_killed)),
                        action('.kill_agent'(monitoredAgent))
                    ]),
    MeanPlan2 = plan(meanPlan,trigger(add(achieve(goal(kill))),(belief(once_killed),not(belief(twice_killed)))),[
                        add(belief(twice_killed)),
                        action('.kill_agent'(monitoredAgent))
                    ]),
    PlanMonitor = plan(planMonitoring,trigger(add(achieve(goal(do_monitor))),true),[
                           action('.monitor'(monitoredAgent,[agent_up])),
                           action('.monitor'(monitoredAgent,Persistent)),
                           action('.send'(monitoredAgent,achieve,goal(do_print_hello)))
                       ]),
    PlanRestart = plan(planRestart,trigger(add(belief(agent_down('monitoredAgent'))),true),[
                           action('.create_agent'('monitoredAgent',[PlanMonitored],init([],[event(add(achieve(goal(do_print_hello))),empty)])))
                       ]),
    create_agent(monitoredAgent,[PlanMonitored],init([],[]),MonitoredAgent),
    create_agent(meanAgent,[MeanPlan1,MeanPlan2],init([],[]),MeanAgent),
    create_agent(monitorAgent,[PlanMonitor,PlanRestart],init([],[event(add(achieve(goal(do_monitor))),empty)]),MonitorAgent),
    System = sys([{runtime4,[MonitoredAgent,MeanAgent,MonitorAgent]}], [], [], [], 0).
    
defsys22all(System) :-
    defsys22(System,[agent_up,agent_down,agent_unblocked]).
defsys22none(System) :-
    defsys22(System,[]).

defsys23(System) :-
    InitN = init([is_a_fine_goal :- is_a_fine_goal],[event(add(achieve(goal(start))),true)]),
    Plan1 = plan(plan1,trigger(add(achieve(goal(start))),true),
                 [
                     predicate(is_a_fine_goal)
                 ]),
    PlanMonitor = plan(planMonitor,trigger(add(achieve(goal(do_monitor))),true),[
                           action('.monitor'(diverging_agent,[agent_up,agent_down,agent_unblocked])),
                           action('.send'(supervisor,achieve,goal(do_supervise)))
                       ]),
    PlanMonitoring = plan(planMonitoring,trigger(add(belief(B)),true),[
                              action('.print'(B))
                 ]),
    PlanSupervisor = plan(planSupervisor,trigger(add(achieve(goal(do_supervise))),true),[
                              action('.supervise'([{runtime4,diverging_agent,[Plan1],InitN}],
                                         sup_policy(limits(1,1000),limits(1,1200),one_for_one)))
                 ]),

    create_agent(monitoring,[PlanMonitor,PlanMonitoring],init([],[event(add(achieve(goal(do_monitor))),empty)]),Monitor),
    create_agent(supervisor,[PlanSupervisor],init([],[]),Supervisor),
    System = sys([{runtime4,[Monitor,Supervisor]}], [], [], [], 0).


%% Checking merge (and monitor)

defsys24_n1(System,Agent) :-
    InitN = init([is_a_fine_goal :- is_a_fine_goal],[]),
    Plan = plan(plan,trigger(add(achieve(goal(start))),true),
                 [
                     predicate(is_a_fine_goal)
                 ]),
    create_agent(Agent,[Plan],InitN,DivergingAgent),
    System = sys([{runtime1,[DivergingAgent]}], [], [], [], 0).

defsys24_n2(System,Agent) :-
    PlanMonitor = plan(planMonitor,trigger(add(achieve(goal(do_monitor))),true),[
                           action('.monitor'(Agent,[agent_up,agent_down,agent_unblocked]))
                       ]),
    PlanMonitoring = plan(planMonitoring,trigger(add(belief(B)),true),[
                              action('.print'(B))
                 ]),
    create_agent(monitoring,[PlanMonitor,PlanMonitoring],init([],[event(add(achieve(goal(do_monitor))),empty)]),Monitor),
    System = sys([{runtime2,[Monitor]}], [], [], [], 0).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%% Network examples


defnet1([Sys1]) :-
    defsys1(Sys1).

defnet2([Sys2]) :-
    defsys2(Sys2).

defnet3([Sys3]) :-
    defsys3(Sys3).

defnet32([Sys3,Sys2]) :-
    defsys3(Sys3),
    defsys2(Sys2).

defnet41([Sys4,Sys1]) :-
    defsys4(Sys4),
    defsys1(Sys1).

%% An example to tests splits, the supervisor and the monitor together.
%

%% Guiding the test execution of the following example
%%
%% Before monitoring has been done, do not permit a split (then we know the supervisor is up too).
guideSp1(notHasSplit(N),_Config,Rule,_Env,_NextConfig,notHasSplit(N1)) :-
    N < 4,
    !,
    ( member(Rule,[[distribution_int,monitor_agent2],[distribution_int,monitor_agent3],[distribution_int,supervise1]]) ->
      N1 is N+1 
    ; N1 is N ),
    \+ member(Rule,[split1,split2,merge]).

%% If a split has not occurred and N==4 (three monitors and one supervisor), only permit a split
guideSp1(notHasSplit(4),_Config,Rule,_Env,_NextConfig,hasSplit) :-
    !,
    ( Rule == split1 ; Rule == split2 ).

%% After a split do not permit splits nor merge
guideSp1(hasSplit,_Config,Rule,_Env,_NextConfig,hasSplit) :-
    not(member(Rule,[split1,split2,merge])).

defnetwork_split1([Sys]) :-
    create_agent(monitor,
     [
         plan(report_dead_agent,trigger(add(belief(agent_down(Agent))),true),
              [
                  action('.print'(Agent))
              ]),
         plan(start_monitoring,trigger(add(achieve(goal(start_monitoring))),true),
              [
                  action('.monitor'('ping',[agent_up,agent_down]))
                  ,action('.monitor'('pong',[agent_up,agent_down]))
                  ,action('.monitor'('supervisor',[agent_up,agent_down]))
              ])
     ],
     init([],[event(add(achieve(goal(start_monitoring))),empty)]),Monitor),
    create_agent(supervisor,
     [
         plan(supervise,trigger(add(achieve(goal(supervise))),true),
              [action(
                   '.supervise'(
                       [{ping,ping,
                         [
                             plan(ping,trigger(add(achieve(goal(ping))),true),
                                  [action('.send'(pong,achieve,goal(pong)))])
                         ],init([],[event(add(achieve(goal(ping))),empty)])},
                        {pong_sup,pong,
                         [
                             plan(pong,trigger(add(achieve(goal(pong))),true),
                                  [action('.send'(ping,achieve,goal(ping)))])
                         ],init([],[])}],
                       sup_policy(_UnblockPolicy,limits(1,1000),rest_for_all)))])
     ],init([],[event(add(achieve(goal(supervise))),empty)]),Supervisor),
    Sys = sys([{ping,[]},{pong_sup,[Monitor,Supervisor]}], [], [], [], 0).

defnetwork_merge1([Sys1,Sys2]) :-
    defsys24_n1(Sys1,diverging_agent),
    defsys24_n2(Sys2,diverging_agent).

defnetwork_merge2([Sys1,Sys2]) :-
    defsys24_n1(Sys1,monitoring),
    defsys24_n2(Sys2,monitoring).

%% Guiding the test execution of the following example
%%
%% Before monitoring has been done, do not permit a merge, and never permit a split
guideMerge(phase1,_Config,Rule,Env,Systems,NextPhase) :-
    \+ member(Rule,[split1,split2,merge]),
    member(Sys,Systems),
    Sys = sys([{runtime2,_}],_,_,_,_),
    ( system_terminated(Sys,Env) ->
      NextPhase = merge
    ;
    NextPhase = phase1
    ).

%% Permit only a merge
guideMerge(merge,_Config,Rule,_Env,_NextConfig,phase3) :-
    !,
    ( Rule == merge ).

%% After a merge do not permit merges nor merge
guideMerge(phase3,_Config,Rule,_Env,_NextConfig,phase3) :-
    not(member(Rule,[split1,split2,merge])).

%% We also need a different termination checker:
terminated24([_,_],_) :-  !, fail.
terminated24([Sys],Env) :- system_terminated(Sys,Env).



    

