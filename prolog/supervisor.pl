%% Supervisor support code.
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

supervision_stopped(System,AgentName,SupRel,NewSystem) :-
    %% This is the restart strategy
    SupRel = sup_rel(SupervisorAgent,SupSet,SupPolicy),
    member(Info,SupSet),
    Info = sup(AgentName,Unblock,Restart),
    time(System,T),
    append(Restart,[T],Restart1),
    Info1 = sup(AgentName,Unblock,Restart1),
    ( restart_limit_exceeded(Info1,SupPolicy) ->
      %% Let's kill the supervisor
      message(normal,
              'Agent ~w died and has exceeded the restart count; killing the supervisor ~w~n',
              [AgentName,SupervisorAgent]),
      kill_agent(System,SupervisorAgent,NewSystem)
    ;
    reset_agent(System,AgentName,System1),
    message(normal,'Agent ~w died and is being restarted~n',[AgentName]),
    select(Info,SupSet,Info1,NewSupSet),
    NewSupRel = sup_rel(SupervisorAgent,NewSupSet,SupPolicy),
    supervisors(System1,Supervisors),
    select(SupRel,Supervisors,NewSupRel,NewSupervisors),
    !,
    setSupervisors(System1,NewSupervisors,System2),
    systemEvents(System2,SystemEvents),
    append(SystemEvents,[systemEvent(restarted_agent,AgentName)],SystemEvents1),
    setSystemEvents(System2,SystemEvents1,System3),
    restart_strategy(System3,AgentName,NewSupRel,NewSystem)
    ).

supervision_divergent(System,AgentName,SupRel,NewSystem) :-
    %% This is the unblock strategy
    SupRel = sup_rel(SupervisorAgent,SupSet,SupPolicy),
    member(Info,SupSet),
    Info = sup(AgentName,Unblock,Restart),
    time(System,T),
    append(Unblock,[T],Unblock1),
    Info1 = sup(AgentName,Unblock1,Restart),
    ( unblock_limit_exceeded(Info1,SupPolicy) ->
      %% Let's instead kill it; eventually the supervisor will be invoked
      Info2 = sup(AgentName,[],Restart),
      select(Info,SupSet,Info2,NewSupSet),
      NewSupRel = sup_rel(SupervisorAgent,NewSupSet,SupPolicy),
      supervisors(System,Supervisors),
      select(SupRel,Supervisors,NewSupRel,NewSupervisors),
      setSupervisors(System,NewSupervisors,System1),
      message(normal,
              'Agent ~w diverged and has exceeded the unblock count; killing it~n',
              [AgentName]),
      kill_agent(System1,AgentName,NewSystem)
    ;
    select(Info,SupSet,Info1,NewSupSet),
    supervisors(System,Supervisors),
    NewSupRel = sup_rel(SupervisorAgent,NewSupSet,SupPolicy),
    select(SupRel,Supervisors,NewSupRel,NewSupervisors),
    setSupervisors(System,NewSupervisors,System1),
    systemEvents(System1,SystemEvents),
    append(SystemEvents,[systemEvent(unblocked_agent,AgentName)],SystemEvents1),
    setSystemEvents(System1,SystemEvents1,System2),
    message(normal,'Agent ~w diverged and is being unblocked~n',[AgentName]),
    unblock_agent(System2,AgentName,NewSystem)
    ).

unblock_limit_exceeded(sup(_,Unblocks,_),SupPolicy) :-
    SupPolicy = sup_policy(limits(MaxUnblocks,Interval),_RestartPolicy,_Strategy),
    check_limit_exceeded(Unblocks,MaxUnblocks,Interval).

restart_limit_exceeded(sup(_,_,Restarts),SupPolicy) :-
    SupPolicy = sup_policy(_UnblockPolicy,limits(MaxRestarts,Interval),_Strategy),
    check_limit_exceeded(Restarts,MaxRestarts,Interval).

check_limit_exceeded([FirstTime|RestTimes],MaxRevivals,Interval) :-
    message(fine_debug,'check_limit_exceeded(~w) MaxRevivals=~w Interval=~w~n',[[FirstTime|RestTimes],MaxRevivals,Interval]),
    MaxRevivals1 is MaxRevivals-1,
    EndInterval is FirstTime+Interval,
    (check_limit_exceeded_in_interval(EndInterval,RestTimes,MaxRevivals1)
    ;
    check_limit_exceeded(RestTimes,MaxRevivals,Interval)).

check_limit_exceeded_in_interval(EndInterval,[Time|_],N) :-
    message(fine_debug,'in_interval: EndInterval=~w Time=~w N=~w~n',[EndInterval,Time,N]),
    N =< 0, Time =< EndInterval, !.
check_limit_exceeded_in_interval(EndInterval,[Time|RestTimes],N) :-
    message(fine_debug,'in_interval: EndInterval=~w Rest=~w Time=~w N=~w~n',[EndInterval,RestTimes,Time,N]),
    Time =< EndInterval, !,
    N1 is N-1, check_limit_exceeded_in_interval(EndInterval,RestTimes,N1).

restart_strategy(System,AgentName,SupRel,NewSystem) :-
    SupRel = sup_rel(_,SupSet,SupPolicy),
    SupPolicy = sup_policy(_UnblockPolicy,_RestartPolicy,Strategy),
    ( Strategy == rest_for_one ->
      followers(SupSet,AgentName,Agents)
    ;
    ( Strategy == rest_for_all ->
      include(notSame(AgentName),SupSet,Agents)
    ;
    Agents=[]
    )
    ),
    (Agents \== [] ->
         message(normal,
                 'Agent ~w died and was restarted; as a consequence agents ~w will be restarted too~n',
                 [AgentName,Agents])
    ;
    true),
    restart_agents(Agents,SupRel,System,NewSystem).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

restart_agents([],_,System,System).
restart_agents([Sup|RestSups],SupRel,System,NewSystem) :-
    SupRel = sup_rel(SupervisorAgent,SupSet,SupPolicy),
    Sup = sup(AgentName,Unblock,Restart),
    time(System,T),
    append(Restart,[T],Restart1),
    NewSup = sup(AgentName,Unblock,Restart1),
    select(Sup,SupSet,NewSup,NewSupSet),
    NewSupRel = sup_rel(SupervisorAgent,NewSupSet,SupPolicy),
    supervisors(System,Supervisors),
    select(SupRel,Supervisors,NewSupRel,NewSupervisors),
    setSupervisors(System,NewSupervisors,System1),
    reset_agent(System1,AgentName,System2),
    systemEvents(System2,SystemEvents),
    append(SystemEvents,[systemEvent(restarted_agent,AgentName)],SystemEvents1),
    setSystemEvents(System2,SystemEvents1,System3),
    restart_agents(RestSups,NewSupRel,System3,NewSystem).

can_be_started([],_System).
can_be_started([AgentRecipe|Rest],System) :-
    containerNames(System,ContainerNames),
    containerNames(System,ContainerNames),
    forall(member({StartContainer,AgentName,Plans,Init},AgentRecipe),
           (
               member(StartContainer,ContainerNames),
               canBeCreated(System,StartContainer,AgentName,Plans,Init)
           )),
    can_be_started(Rest,System).

start_agents(System,[],System).
start_agents(System,[{StartContainer,AgentName,Plans,Init}|Rest],NewSystem) :-
    create_agent_in_system(System,StartContainer,AgentName,Plans,Init,System1),
    start_agents(System1,Rest,NewSystem).

agent_names([],[]).
agent_names([{_StartContainer,AgentName,_Plans,_Init}|Rest],[AgentName|RestAgentNames]) :-
    agent_names(Rest,RestAgentNames).

followers([Sup|Rest],AgentName,Rest) :-
    Sup = sup(AgentName,_Unblock,_Restart),
    !.
followers([_|Rest],AgentName,Followers) :-
    followers(Rest,AgentName,Followers).
                       
notSame(AgentName,sup(Agent,_Unblock,_Restart)) :-
    AgentName \== Agent.
supervised(AgentName,[sup(AgentName,_,_)|_]) :- !.
supervised(AgentName,[_|Rest]) :-
    supervised(AgentName,Rest).
    
kill_agents([],System,System).
kill_agents([First|RestAgents],System,NewSystem) :-
    kill_agent(System,First,System1),
    kill_agents(RestAgents,System1,NewSystem).

in_supset(SupSet,{_,Agents}) :-
    member(Agent,Agents),
    name(Agent,AgentName),
    member(sup(AgentName,_,_),SupSet).

not_in_supset(SupSet,Container) :-
    \+ in_supset(SupSet,Container).

supervised_by(AgentName,sup_rel(AgentName1,_,_)) :-
    AgentName == AgentName1.

agentNameFromSup(sup(Ag,_,_),Ag).

supervisedAgents(sup_rel(_,SupSet,_),SupervisedAgents) :-
    maplist(agentNameFromSup,SupSet,SupervisedAgents).

make_info(Name,sup(Name,[],[])).

