%% The robot and beer drinking owner example
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

defOwner(Owner) :-
    OwnerPlan1 = plan(owner_get_beer,trigger(add(achieve(goal(get(beer)))),true),
                   [
                       action('.send'(robot,achieve,goal(has(owner,beer))))
                   ]),
OwnerPlan2 = plan(owner_has_beer,trigger(add(belief(has(owner,beer))),true),
                   [
                       achieve(goal(drink(beer)))
                   ]),
OwnerPlan3 = plan(owner_no_has_beer,trigger(del(belief(has(owner,beer))),true),
                   [
                       achieve(goal(get(beer)))
                   ]),
OwnerPlan4 = plan(owner_drink_beer1,trigger(add(achieve(goal(drink(beer)))),belief(has(owner,beer))),
                   [
                       action(owner_sip(beer)),
                       achieve(goal(drink(beer)))
                   ]),
OwnerPlan5 = plan(owner_drink_beer2,trigger(add(achieve(goal(drink(beer)))),not(belief(has(owner,beer)))),
                   [
                   ]),
OwnerPlan6 = plan(owner_print_msg,trigger(add(belief(msg(M))),true),
                  [
                      action('.print'(M))
                      ,del(belief(msg(M)))
                  ]),
create_agent(owner,[OwnerPlan1,OwnerPlan2,OwnerPlan3,OwnerPlan4,OwnerPlan5,OwnerPlan6],init([],[event(add(achieve(goal(get(beer)))),empty)]),Owner).

%% We should have use the source label here

defSupermarket(Supermarket) :-
    SupermarketPlan1 = plan(supermarket_plan,trigger(add(achieve(goal(order(Prod,Qtd)))),true),
                        [
                            test(belief(last_order_id(N))),
                            del(belief(last_order_id(N))),
                            add(belief(last_order_id('+'(N,1)))),
                            action(supermarket_deliver(Prod,Qtd)),
                            action('.send'(robot, tell, belief(delivered(Prod,Qtd,'+'(N,1)))))
                        ]),
    create_agent(supermarket,[SupermarketPlan1],init([belief(last_order_id(1))],[]),Supermarket).

defRobot(Robot,Limit) :-
    RobotPlan1 = plan(robot_has_beer_1,trigger(add(achieve(goal(has(owner,beer)))), (belief(available(beer,fridge)),not(too_much(beer)))),
                  [
                      achieve(goal(at(robot,fridge))),
                      action(robot_open(fridge)),
                      action(robot_get(beer)),
                      action(robot_close(fridge)),
                      achieve(goal(at(robot,owner))),
                      action(robot_hand_in_beer(beer)),
                      predicate('.date'(YY,MM,DD)),
                      predicate('.time'(HH,NN,SS)),
                      add(belief(consumed(YY,MM,DD,HH,NN,SS,beer)))
                  ]),

RobotPlan2 = plan(robot_has_beer2,trigger(add(achieve(goal(has(owner,beer)))), not(belief(available(beer,fridge)))),
                  [
                      action('.send'(supermarket,achieve,goal(order(beer,5)))),
                      achieve(goal(at(robot,fridge)))
                  ]),

RobotPlan3 = plan(robot_has_beer3,trigger(add(achieve(goal(has(owner,beer)))), too_much(beer)),
                  [
                      action('.send'(owner,tell,belief(msg("beer limit exceeded"))))
                  ]),

RobotPlan4 = plan(robot_at1,trigger(add(achieve(goal(at(robot,P)))),belief(at(robot,P))),[]),

RobotPlan5 = plan(robot_at2,trigger(add(achieve(goal(at(robot,P)))),not(belief(at(robot,P)))),
                  [
                      action(robot_move_towards(P)),
                      achieve(goal(at(robot,P)))
                  ]),

RobotPlan6 = plan(robot_delivered,trigger(add(ann(belief(delivered(beer,_Qtd,_OrderId)),supermarket)),true),
                  [
                      add(belief(available(beer,fridge))),
                      achieve(goal(has(owner,beer)))
                  ]),

RobotPlan7 = plan(robot_stock1,trigger(add(belief(stock(beer,0))), belief(available(beer,fridge))),
                  [
                      del(belief(available(beer,fridge)))
                  ]),

RobotPlan8 = plan(robot_stock2,trigger(add(belief(stock(beer,N))), (N>0, not(belief(available(beer,fridge))))),
                  [
                      add(belief(available(beer,fridge)))
                  ]),
TooMuchPred =
    (too_much(B) :- (belief(limit(beer,BeerLimit)),'.date'(YY,MM,DD),'.count'(belief(consumed(YY,MM,DD,_,_,_,B)),QtdB),QtdB >= BeerLimit)),
    create_agent(robot,[RobotPlan1,RobotPlan2,RobotPlan3,RobotPlan4,RobotPlan5,RobotPlan6,RobotPlan7,RobotPlan8],init([belief(limit(beer,Limit)),TooMuchPred],[]),Robot).

defOwnerExample(Limit,System) :-
    defOwner(Owner),
    defSupermarket(Supermarket),
    defRobot(Robot,Limit),
    System = sys([{runtime,[Owner,Supermarket,Robot]}], [], [], [], 0).

tstRobot(Actions) :-
    tstRobot(Actions,info).

tstRobotDebug(Actions) :-
    tstRobot(Actions,fine_debug).

tstRobotRun(Actions) :-
    tstRobot(Actions,normal).

tstRobot(Actions,Level) :-
    logToFile([outLogLevel(Level),logLogLevel(Level)],'robot_run.txt',Params),
    %% logToFile([],'robot_run.txt',Params),
    robotEnv(Env),
    run_system(defOwnerExample(5),_,History,[initial_env(Env)|Params]),
    actions_history(History,Actions).

benchRobot(Limit) :-
    robotEnv(Env),
    run_system(defOwnerExample(Limit),_,_,[outLogLevel(error),{bench,10},initial_env(Env)]).

benchRobot(Limit,EndLimit) :-
    Limit > EndLimit,
    !.
benchRobot(Limit,EndLimit) :-
    benchRobot(Limit),
    NewLimit is Limit+1,
    benchRobot(NewLimit,EndLimit).

%% Assert what a sip action is.
isSip(action(owner_sip(beer))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Environment handling
%%
%% State = state(RobotPos,Fridge,Owner)
%% Fridge = fridge(Door,NumBeers)
%% Door = open|closed
%% Owner = beer|no_beer
%%

robotEnv(env(InitialEnv,PP)) :-
    InitialEnv = state(somewhere,fridge(closed,2),no_beer),
    PP = robotPercepts.

robotPercepts(state(Pos,fridge(Door,NumBeers),Owner),Perceptions) :-
    RobotPos = {robot,belief(at(robot,Pos))},

    %% When the fridge door is open the robot can perceive the number of beers in the fridge
    (Door==open -> Stock=[{robot,belief(stock(beer,NumBeers))}] ;  Stock=[]),
    (Owner==beer -> HasBeer=[{owner,belief(has(owner,beer))}] ; HasBeer=[]),

    append(HasBeer,Stock,Percepts1),
    Perceptions = [RobotPos|Percepts1].
    
%% Owner sips beer
owner_sip(beer,state(Pos,Fridge,beer),state(Pos,Fridge,no_beer)).

%% Robot moving towards -- arrives immediately
robot_move_towards(Pos,state(_,Fridge,Owner),state(Pos,Fridge,Owner)).

%% Robot opening fridge
robot_open(fridge,state(fridge,fridge(closed,NumBeers),Owner),state(fridge,fridge(open,NumBeers),Owner)).

%% Robot getting a beer
robot_get(beer,state(RobotPos,fridge(open,NumBeers),Owner),state(RobotPos,fridge(open,RemainingBeers),Owner)) :-
    NumBeers > 0,
    RemainingBeers is NumBeers-1.

%% Robot opening fridge
robot_close(fridge,state(RobotPos,fridge(open,NumBeers),Owner),state(RobotPos,fridge(closed,NumBeers),Owner)).

%% Robot giving a beer to the owner
robot_hand_in_beer(beer,state(RobotPos,Fridge,no_beer),state(RobotPos,Fridge,beer)).

%% Supermarket deliver product to fridge
supermarket_deliver(beer,N,state(RobotPos,fridge(Door,NumBeers),Owner),state(RobotPos,fridge(Door,NumBeersNew),Owner)) :-
    NumBeersNew is NumBeers+N.


