%% Boilerplate code
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

%% Contains predicates for taking apart and composing agents, multi-agent systems
%% and networks.
%% 
%% See agent.pl for a definition of the agent state.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Agent boilerplate code
    
plans(agent(Ag,_,_,_,_,_,_),Plans) :-
    plans(Ag,Plans).
plans(ag(_,Plans,_),Plans).

init(agent(Ag,_,_,_,_,_,_),Init) :-
    init(Ag,Init).
init(ag(_,_,Init),Init).

true_beliefs(agent(Ag,_,_,_,_,_,_),Beliefs) :-
    true_beliefs(Ag,Beliefs).
true_beliefs(ag(Beliefs,_,_),TrueBeliefs) :-
    include(is_belief,Beliefs,TrueBeliefs).

is_belief(ann(belief(_),_)) :- !.
is_belief(belief(_)).

beliefs(agent(Ag,_,_,_,_,_,_),Beliefs) :-
    beliefs(Ag,Beliefs).
beliefs(ag(Beliefs,_,_),Beliefs).
setBeliefs(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
          Beliefs,
           agent(AgNew,Name,Circ,Comm,CycleState,Step,Selectors)) :-
    setBeliefs(Ag,Beliefs,AgNew).
setBeliefs(ag(_Beliefs,Plans,Init),Beliefs,ag(Beliefs,Plans,Init)).
addBelief(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
          Belief,
          agent(AgNew,Name,Circ,Comm,CycleState,Step,Selectors)) :-
    addBelief(Ag,Belief,AgNew).
addBelief(ag(Beliefs,Plans,Init),Belief,ag([Belief|Beliefs],Plans,Init)) :-
    ground(Belief).
delBelief(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
          Belief,
          agent(AgNew,Name,Circ,Comm,CycleState,Step,Selectors)) :-
    delBelief(Ag,Belief,AgNew).
delBelief(ag(Beliefs,Plans,Init),Belief,ag(NewBeliefs,Plans,Init)) :-
    ground(Belief),
    delete(Beliefs,Belief,NewBeliefs).

name(agent(_,Name,_,_,_,_,_),Name).

eventSelector(agent(_Ag,_Name,_Circ,_Comm,_CycleState,_Step,Selectors),EvSel) :-
    eventSelector(Selectors,EvSel).
eventSelector(selectors(EvSel,_,_,_,_),EvSel).
appSelector(agent(_Ag,_Name,_Circ,_Comm,_CycleState,_Step,Selectors),AppSel) :-
    appSelector(Selectors,AppSel).
appSelector(selectors(_,AppSel,_,_,_),AppSel).
intentionSelector(agent(_Ag,_Name,_Circ,_Comm,_CycleState,_Step,Selectors),IntentionSel) :-
    intentionSelector(Selectors,IntentionSel).
intentionSelector(selectors(_,_,IntentionSel,_,_),IntentionSel).
sociallyAcceptableSelector(agent(_Ag,_Name,_Circ,_Comm,_CycleState,_Step,Selectors),SociallyAcceptable) :-
    sociallyAcceptableSelector(Selectors,SociallyAcceptable).
sociallyAcceptableSelector(selectors(_,_,_,SociallyAcceptable,_),SociallyAcceptable).
smSelector(agent(_Ag,_Name,_Circ,_Comm,_CycleState,_Step,Selectors),SmSelector) :-
    smSelector(Selectors,SmSelector).
smSelector(selectors(_,_,_,_,SmSelector),SmSelector).

step(agent(_Ag,_Name,_Circ,_Comm,_CycleState,Step,_Selectors),Step).
setStep(agent(Ag,Name,Circ,Comm,CycleState,_Step,Selectors),
        Step,
        agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors)).

events(agent(_Ag,_Name,Circ,_Comm,_CycleState,_Step,_Selectors),Events) :-
    events(Circ,Events).
events(cs(_Intentions,Events,_Actions),Events).
addEvents(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
          Events,
         agent(Ag,Name,Circ1,Comm,CycleState,Step,Selectors)) :-
    addEvents(Circ,Events,Circ1).
addEvents(cs(Intentions,Events,Actions),NewEvents,cs(Intentions,AllEvents,Actions)) :-
    append(Events,NewEvents,AllEvents).
deleteEvent(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
            Event,
            agent(Ag,Name,Circ1,Comm,CycleState,Step,Selectors)) :-
    delEvent(Circ,Event,Circ1).
delEvent(cs(Intentions,[Event|RestEvents],Actions),Event,cs(Intentions,RestEvents,Actions)).
setEvents(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
          Events,
          agent(Ag,Name,Circ1,Comm,CycleState,Step,Selectors)) :-
    setEvents(Circ,Events,Circ1).
setEvents(cs(Intentions,_Events,Actions),Events,cs(Intentions,Events,Actions)).

intentions(agent(_Ag,_Name,Circ,_Comm,_CycleState,_Step,_Selectors),Intentions) :-
    intentions(Circ,Intentions).
intentions(cs(Intentions,_Events,_Actions),Intentions).
setIntentions(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
         Intentions,
         agent(Ag,Name,Circ1,Comm,CycleState,Step,Selectors)) :-
    setIntentions(Circ,Intentions,Circ1).
setIntentions(cs(_Intentions,Events,Actions),Intentions,cs(Intentions,Events,Actions)).

event(agent(_Ag,_Name,_Circ,_Comm,CycleState,_Step,_Selectors),Event) :-
    event(CycleState,Event).
event(s(_RelevantPlans,_ApplicablePlans,_Intention,Event,_ApplicablePlan),Event).
setEvent(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
         Event,
         agent(Ag,Name,Circ,Comm,CycleState1,Step,Selectors)) :-
    setEvent(CycleState,Event,CycleState1).
setEvent(s(RelevantPlans,ApplicablePlans,Intention,_Event,ApplicablePlan),
         Event,
         s(RelevantPlans,ApplicablePlans,Intention,Event,ApplicablePlan)).

intention(agent(_Ag,_Name,_Circ,_Comm,CycleState,_Step,_Selectors),Intention) :-
    intention(CycleState,Intention).
intention(s(_RelevantPlans,_ApplicablePlans,Intention,_Event,_ApplicablePlan),Intention).
setIntention(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
         Intention,
         agent(Ag,Name,Circ,Comm,CycleState1,Step,Selectors)) :-
    setIntention(CycleState,Intention,CycleState1).
setIntention(s(RelevantPlans,ApplicablePlans,_Intention,Event,ApplicablePlan),
         Intention,
         s(RelevantPlans,ApplicablePlans,Intention,Event,ApplicablePlan)).

actions(agent(_Ag,_Name,Circ,_Comm,_CycleState,_Step,_Selectors),Actions) :-
    actions(Circ,Actions).
actions(cs(_Intentions,_Events,Actions),Actions).
setActions(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
         Actions,
         agent(Ag,Name,Circ1,Comm,CycleState,Step,Selectors)) :-
    setActions(Circ,Actions,Circ1).
setActions(cs(Intentions,Events,_Actions),Actions,cs(Intentions,Events,Actions)).

relevantPlans(agent(_Ag,_Name,_Circ,_Comm,CycleState,_Step,_Selectors),RelevantPlans) :-
    relevantPlans(CycleState,RelevantPlans).
relevantPlans(s(RelevantPlans,_ApplicablePlans,_Intention,_Event,_ApplicablePlan),RelevantPlans).

setRelevantPlans(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
         RelevantPlans,
         agent(Ag,Name,Circ,Comm,CycleState1,Step,Selectors)) :-
    setRelevantPlans(CycleState,RelevantPlans,CycleState1).
setRelevantPlans(s(_RelevantPlans,ApplicablePlans,Intention,Event,ApplicablePlan),
         RelevantPlans,
         s(RelevantPlans,ApplicablePlans,Intention,Event,ApplicablePlan)).

applicablePlans(agent(_Ag,_Name,_Circ,_Comm,CycleState,_Step,_Selectors),ApplicablePlans) :-
    applicablePlans(CycleState,ApplicablePlans).
applicablePlans(s(_RelevantPlans,ApplicablePlans,_Intention,_Event,_ApplicablePlan),ApplicablePlans).

setApplicablePlans(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
         ApplicablePlans,
         agent(Ag,Name,Circ,Comm,CycleState1,Step,Selectors)) :-
    setApplicablePlans(CycleState,ApplicablePlans,CycleState1).
setApplicablePlans(s(RelevantPlans,_ApplicablePlans,Intention,Event,ApplicablePlan),
         ApplicablePlans,
         s(RelevantPlans,ApplicablePlans,Intention,Event,ApplicablePlan)).

applicablePlan(agent(_Ag,_Name,_Circ,_Comm,CycleState,_Step,_Selectors),ApplicablePlan) :-
    applicablePlan(CycleState,ApplicablePlan).
applicablePlan(s(_RelevantPlans,_ApplicablePlans,_Intention,_Event,ApplicablePlan),ApplicablePlan).

setApplicablePlan(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
         ApplicablePlan,
         agent(Ag,Name,Circ,Comm,CycleState1,Step,Selectors)) :-
    setApplicablePlan(CycleState,ApplicablePlan,CycleState1).
setApplicablePlan(s(RelevantPlans,ApplicablePlans,Intention,Event,_ApplicablePlan),
         ApplicablePlan,
         s(RelevantPlans,ApplicablePlans,Intention,Event,ApplicablePlan)).

counter(agent(_,_,_,Comm,_,_,_),Counter) :-
    counter(Comm,Counter).
counter(comm(_,_,_,Counter,_),Counter).
incCounter(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
           agent(Ag,Name,Circ,Comm1,CycleState,Step,Selectors)) :-
    incCounter(Comm,Comm1).
incCounter(comm(Ins,Outs,SI,Counter,Perceptions),comm(Ins,Outs,SI,Counter1,Perceptions)) :-
    Counter1 is Counter+1.

si(agent(_,_,_,Comm,_,_,_),SI) :-
    si(Comm,SI).
si(comm(_,_,SI,_,_),SI).
setSI(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
      SI,
      agent(Ag,Name,Circ,Comm1,CycleState,Step,Selectors)) :-
    setSI(Comm,SI,Comm1).
setSI(comm(Ins,Outs,_SI,Counter,Perceptions),SI,comm(Ins,Outs,SI,Counter,Perceptions)).

outs(agent(_,_,_,Comm,_,_,_),Outs) :-
    outs(Comm,Outs).
outs(comm(_,Outs,_,_,_),Outs).
setOuts(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
      Outs,
      agent(Ag,Name,Circ,Comm1,CycleState,Step,Selectors)) :-
    setOuts(Comm,Outs,Comm1).
setOuts(comm(Ins,_Outs,SI,Counter,Perceptions),Outs,comm(Ins,Outs,SI,Counter,Perceptions)).

ins(agent(_,_,_,Comm,_,_,_),Ins) :-
    ins(Comm,Ins).
ins(comm(Ins,_,_,_,_),Ins).
setIns(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
      Ins,
      agent(Ag,Name,Circ,Comm1,CycleState,Step,Selectors)) :-
    setIns(Comm,Ins,Comm1).
setIns(comm(_Ins,Outs,SI,Counter,Perceptions),Ins,comm(Ins,Outs,SI,Counter,Perceptions)).

perceptions(agent(_,_,_,Comm,_,_,_),Perceptions) :-
    perceptions(Comm,Perceptions).
perceptions(comm(_,_,_,_,Perceptions),Perceptions).
setPerceptions(agent(Ag,Name,Circ,Comm,CycleState,Step,Selectors),
      Perceptions,
      agent(Ag,Name,Circ,Comm1,CycleState,Step,Selectors)) :-
    setPerceptions(Comm,Perceptions,Comm1).
setPerceptions(comm(Ins,Outs,SI,Counter,_Perceptions),Perceptions,comm(Ins,Outs,SI,Counter,Perceptions)).

new_mid(Agent,Mid,NewAgent) :-
    name(Agent,Name),
    counter(Agent,Counter),
    Mid = mid(Name,Counter),
    incCounter(Agent,NewAgent).

create_agent(AgentName,Plans,Agent) :-
    create_agent(AgentName,Plans,init([],[]),Agent).

create_agent(AgentName,Plans,Init,Agent) :-
    Init = init(InitialBeliefs,InitialEvents),
    Ag = ag(InitialBeliefs,Plans,Init),
    Agent = agent(Ag,AgentName,cs([],InitialEvents,[]),comm([],[],[],0,none),s(_,_,_,_,_),perceive,selectors(stdEvSel,stdAppSel,stdIntentionSel,stdSociallyAcceptable,stdSmSelector)).

%% We permit recreating stopped processes with the same code and init
canBeCreated(System,ContainerName,Name,_Plans,_Init) :-
    ( agent_through_name(System,Name,Agent) ->
      agent_in_container(System,Name,ContainerName),
      step(Agent,stopped)
    ;
    true
    ).

create_agent_in_system(System,ContainerName,NewName,Plans,Init,NewSystem) :-
    create_agent(NewName,Plans,Init,NewAgent),
    addAgent(System,ContainerName,NewAgent,System1),
    systemEvents(System1,SystemEvents),
    append(SystemEvents,[systemEvent(created_agent,NewName)],NewSystemEvents),
    setSystemEvents(System1,NewSystemEvents,NewSystem),
    message(normal,'Agent ~w was created.~n',[NewName]).

initialize_agent(Agent,InitializedAgent) :-
    name(Agent,AgentName),
    init(Agent,Init),
    Init = init(InitialBeliefs,InitialEvents),
    plans(Agent,Plans),
    Ag = ag(InitialBeliefs,Plans,Init),
    InitializedAgent = agent(Ag,AgentName,cs([],InitialEvents,[]),comm([],[],[],0,[]),s(_,_,_,_,_),procMsg,selectors(stdEvSel,stdAppSel,stdIntentionSel,stdSociallyAcceptable,stdSmSelector)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% System boilerplate code

di(sys(Di,_Mo,_Su,_SE,_T),Di).
setDi(sys(_Di,Mo,Su,SE,T),NewDi,sys(NewDi,Mo,Su,SE,T)).

systemEvents(sys(_Di,_Mo,_Su,SE,_T),SE).
setSystemEvents(sys(Di,Mo,Su,_SE,T),SE,sys(Di,Mo,Su,SE,T)).

monitors(sys(_Di,Mo,_Su,_SE,_T),Mo).
setMonitors(sys(Di,_Mo,Su,SE,T),Mo,sys(Di,Mo,Su,SE,T)).

supervisors(sys(_Di,_Mo,Su,_SE,_T),Su).
setSupervisors(sys(Di,Mo,_Su,SE,T),Su,sys(Di,Mo,Su,SE,T)).

time(sys(_Di,_Mo,_Su,_SE,T),T).
setTime(sys(Di,Mo,Su,SE,_T),T,sys(Di,Mo,Su,SE,T)).

addAgent(System,ContainerName,CreatedAgent,NewSystem) :-
    di(System,Di),
    member(Container,Di),
    Container = {ContainerName,Agents},
    name(CreatedAgent,Name),
    ( (member(Agent,Agents), name(Agent,Name)) ->
      select(Agent,Agents,CreatedAgent,NewAgents)
    ;
    NewAgents = [CreatedAgent|Agents]
    ),
    select(Container,Di,{ContainerName,NewAgents},NewDi),
    setDi(System,NewDi,NewSystem),
    !.

names(sys(Di,_,_,_,_),Names) :-
    names(Di,Names).
names([],[]).
names([{_,Agents}|Rest],Names) :-
    names(Agents,AgentNames),
    names(Rest,RestNames),
    append(AgentNames,RestNames,Names).
names([Agent|RestAgents],[AgentName|RestNames]) :-
    name(Agent,AgentName),
    names(RestAgents,RestNames).

containerNames(sys(Di,_,_,_,_),ContainerNames) :-
    containerNames(Di,ContainerNames).
containerNames([],[]).
containerNames([{ContainerName,_Agents}|Rest],[ContainerName|ContainerNames]) :-
    containerNames(Rest,ContainerNames).

outMessages(sys(Di,_,_,_,_),OutMessages) :-
    outMessages(Di,OutMessages).
outMessages([],[]).
outMessages([{_,Agents}|Rest],OutMessages) :-
    outMessages(Agents,AgentOutMessages),
    outMessages(Rest,RestOutMessages),
    append(AgentOutMessages,RestOutMessages,OutMessages).
outMessages([Agent|RestAgents],OutMessages) :-
    outs(Agent,AgentOutMessages),
    outMessages(RestAgents,RestOutMessages),
    append(AgentOutMessages,RestOutMessages,OutMessages).

agents_in_system(System,Agents) :-
    di(System,Di),
    agents_in_system(Di,Agents).
agents_in_system([],[]).
agents_in_system([{_,Agents}|Rest],AllAgents) :-
    agents_in_system(Rest,RestAgents),
    append(Agents,RestAgents,AllAgents).

system_agent(System,ContainerName,AgentName,Agent) :-
    di(System,Di),
    member({ContainerName,Agents},Di),
    member(Agent,Agents),
    name(Agent,AgentName).

agent_in_system(System,Agent) :-
    di(System,Di),
    agent_in_di(Di,Agent).

agent_in_di(Di,Agent) :-
    member({_ContainerName,Agents},Di),
    member(Agent,Agents).

agent_through_name(System,AgentName,Agent) :-
    di(System,Di),
    agent_through_name_in_di(Di,AgentName,Agent).

agent_through_name_in_di(Di,AgentName,Agent) :-
    member({_ContainerName,Agents},Di),
    member(Agent,Agents),
    name(Agent,AgentName).

agent_in_container(System,AgentName,ContainerName) :-
    di(System,Di),
    member({ContainerName,Agents},Di),
    member(Agent,Agents),
    name(Agent,AgentName).

update_agent(System,Agent,NewSystem) :-
    di(System,Di),
    update_agent_in_di(Di,Agent,NewDi),
    setDi(System,NewDi,NewSystem).

update_agent_in_di(Di,Agent,NewDi) :-
    member(Container,Di),
    Container = {ContainerName,Agents},
    name(Agent,AgentName),
    member(OldAgent,Agents),
    name(OldAgent,AgentName),
    select(OldAgent,Agents,Agent,NewAgents),
    select(Container,Di,{ContainerName,NewAgents},NewDi),
    !.

reset_agent(System,AgentName,NewSystem) :-
    agent_through_name(System,AgentName,Agent),
    initialize_agent(Agent,InitializedAgent),
    update_agent(System,InitializedAgent,NewSystem),
    message(normal,'Agent ~w was reset.~n',[AgentName]).

unblock_agent(System,AgentName,NewSystem) :-
    ( agent_through_name(System,AgentName,Agent) ->
      intentions(Agent,Intentions),
      ( Intentions == [] ->
        NewIntentions = []
      ;
      Intentions = [_|NewIntentions]
      ),
      setIntentions(Agent,NewIntentions,Agent1),
      setStep(Agent1,perceive,UnblockedAgent),
      update_agent(System,UnblockedAgent,NewSystem),
      message(nomal,'Agent ~w was unblocked.~n',[AgentName])
    ;
    NewSystem = System
    ).

kill_agent(System,AgentName,NewSystem) :-
    ( agent_through_name(System,AgentName,Agent) ->
      setStep(Agent,stopping,KilledAgent),
      update_agent(System,KilledAgent,NewSystem),
      message(normal,'Agent ~w is being killed.~n',[AgentName])
    ;
    NewSystem = System
    ).

agent_in_config(AgentName,Agent,Agent) :-
    name(Agent,AgentName), !.
agent_in_config(AgentName,System,Agent) :-
    agent_through_name(System,AgentName,Agent), !.
    
update_agent_in_config(OldAgent,Agent,Agent) :-
    name(OldAgent,_), !.
update_agent_in_config(System,Agent,NewSystem) :-
    update_agent(System,Agent,NewSystem), !.

