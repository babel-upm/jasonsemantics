%% The semantics of a multi-agent system
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

%% Defines the multi-agent transition rules.
%%
%%
%% A system:
%%
%% sys(Di, Mo, Su, SE, T)
%%

%% An internal action of one of the agents of the system
system_step(System,[agent_int,Step],NewSystem,[],Env,Env) :-
    noSystemEvent(System),
    di(System,Di),
    randomized_member(Container,Di),
    Container = {_,Agents},
    actionsEmpty(Container),
    randomized_member(Agent,Agents),
    \+ agent_terminated(Agent,Env),
    agent_step(Agent,Step,NewAgent,_,Env),
    update_agent(System,NewAgent,System2),
    time(System2,Time),
    Time1 is Time+1,
    setTime(System2,Time1,NewSystem).



%% Meta rule for distributed action
system_step(System,Rule,NewSystem,OutActions,Env,Env) :-
    noSystemEvent(System),
    di(System,Di),
    randomized_member({ContainerName,Agents},Di),
    randomized_member(Agent,Agents),
    actions(Agent,Actions),
    member(Action,Actions),
    distAction(Action),
    !,
    distAction_step(ContainerName,Agent,Action,Actions,System,Rule,NewSystem,OutActions).

system_step(System,msgExchg,NewSystem,[],Env,Env) :-
    noSystemEvent(System),
    agent_in_system(System,Agent),
    name(Agent,Sender),
    outs(Agent,Outs),
    member(Msg,Outs),
    Msg = msg(Mid,Id,Ilf,Cnt),
    name(Agent,SenderName),
    delete(Outs,Msg,Outs1),
    setOuts(Agent,Outs1,Agent1),
    update_agent(System,Agent1,System1),
    agent_through_name(System1,Id,ReceiverAgent),
    DeliveredMsg = msg(Mid,SenderName,Ilf,Cnt),
    ins(ReceiverAgent,Ins),
    setIns(ReceiverAgent,[DeliveredMsg|Ins],ReceiverAgent1),
    message(normal,'Agent ~w sent a message ~w to agent ~w~n',[Sender,Msg,Id]),
    update_agent(System1,ReceiverAgent1,NewSystem).

%% Accepting non-special actions
system_step(System,do_action,NewSystem,[Action],Env,Env) :-
    noSystemEvent(System),
    di(System,Di),
    randomized_member({_,Agents},Di),
    randomized_member(Agent,Agents),
    actions(Agent,Actions),
    member(Action,Actions),
    \+distAction(Action),
    delete(Actions,Action,NewActions),
    setActions(Agent,NewActions,Agent1),
    update_agent(System,Agent1,NewSystem),
    name(Agent,AgentName),
    message(normal,'Agent ~w did action ~w~n',[AgentName,Action]).

%% An agent is diverging
system_step(System,agent_diverge,NewSystem,[],Env,Env) :-
    noSystemEvent(System),
    di(System,Di),
    member(Container,Di),
    Container = {_,Agents},
    actionsEmpty(Container),
    member(Agent,Agents),
    step(Agent,diverges),
    name(Agent,AgentName),
    setStep(Agent,diverged,NewAgent),
    systemEvents(System,SystemEvents),
    append(SystemEvents,[systemEvent(divergent_agent,AgentName)],SystemEvents1),
    setSystemEvents(System,SystemEvents1,System1),
    update_agent(System1,NewAgent,NewSystem),
    message(normal,'Agent ~w diverged.~n',[AgentName]).

%% An agent is stopping
system_step(System,agent_stop,NewSystem,[],Env,Env) :-
    noSystemEvent(System),
    di(System,Di),
    member(Container,Di),
    Container = {_,Agents},
    actionsEmpty(Container),
    member(Agent,Agents),
    step(Agent,stopping),
    name(Agent,AgentName),
    setStep(Agent,stopped,NewAgent),
    systemEvents(System,SystemEvents),
    append(SystemEvents,[systemEvent(dead_agent,AgentName),systemEvent(stopped_agent,AgentName)],SystemEvents1),
    setSystemEvents(System,SystemEvents1,System1),
    update_agent(System1,NewAgent,NewSystem),
    message(normal,'Agent ~w stopped.~n',[AgentName]).

%% This is the merged rule (resulting in rule instances as the created rule)
%% for handling monitor system events.
system_step(System,monitor_notification,NewSystem,[],Env,Env) :-
    systemEvents(System,[SystemEvent|RestSystemEvents]),
    isMonitorEvent(SystemEvent),
    !,
    createBelief(SystemEvent,Belief),
    systemEventType(SystemEvent,EventType),
    responsableAgent(SystemEvent,AgentName),
    updateBeliefs(EventType,AgentName,Belief,System,System1),
    monitors(System1,Monitors),
    updateMonitors(Monitors,EventType,AgentName,Monitors1),
    setMonitors(System1,Monitors1,System2),
    setSystemEvents(System2,RestSystemEvents,NewSystem).

%% An agent has stopped and it is not being supervised.
%% However, if it is itself supervising we should stop any agents it does supervise.
    system_step(System,stop1,NewSystem,[],Env,Env) :-
    systemEvents(System,[systemEvent(stopped_agent,AgentName)|RestSystemEvents]),
    setSystemEvents(System,RestSystemEvents,System1),
    supervisors(System1,Supervisors),
    foreach(member(sup_rel(_,SupSet,_),Supervisors),\+supervised(AgentName,SupSet)),
    !,
    include(supervised_by(AgentName),Supervisors,SupervisedBy),
    exclude(supervised_by(AgentName),Supervisors,NotSupervisedBy),
    setSupervisors(System1,NotSupervisedBy,System2),
    maplist(supervisedAgents,SupervisedBy,SupervisedAgentsLists),
    flatten(SupervisedAgentsLists,SupervisedAgents),
    (SupervisedAgents \== [] ->
         message(normal,'Agent ~w has stopped; supervised agents ~w are being killed~n',[AgentName,SupervisedAgents])
    ;
    true),
    kill_agents(SupervisedAgents,System2,NewSystem).
    
%% An agent has stopped and it is being supervised.
%% Otherwise we try to restart it. If restart fails, we kill the supervisor (which eventually
%% causes all the agents in its supervision set to being killed too).
%% As in stop1, if the agent is also a supervisor, we kill all the agents in the supervision set
%% (before restarting the supervisor).
system_step(System,stop2,NewSystem,[],Env,Env) :-
    systemEvents(System,[systemEvent(stopped_agent,AgentName)|RestSystemEvents]),
    setSystemEvents(System,RestSystemEvents,System1),
    supervisors(System1,Supervisors),
    !,
    include(supervised_by(AgentName),Supervisors,SupervisedBy),
    exclude(supervised_by(AgentName),Supervisors,NotSupervisedBy),
    setSupervisors(System1,NotSupervisedBy,System2),
    maplist(supervisedAgents,SupervisedBy,SupervisedAgentsLists),
    flatten(SupervisedAgentsLists,SupervisedAgents),
    kill_agents(SupervisedAgents,System2,System3),
    (SupervisedAgents \== [] ->
         message(normal,'Agent ~w has stopped; supervised agents ~w are being killed~n',[AgentName,SupervisedAgents])
    ;
    true),
    member(SupRel,Supervisors),
    SupRel = sup_rel(SupervisorAgent,SupSet,_SupPolicy),
    supervised(AgentName,SupSet), 
    message(normal,'Agent ~w has stopped; supervisor ~w will act.~n',[AgentName,SupervisorAgent]),
    supervision_stopped(System3,AgentName,SupRel,NewSystem).

%% An agent has diverged, and is not being supervised; do nothing.
system_step(System,unblock1,NewSystem,[],Env,Env) :-
    systemEvents(System,[systemEvent(divergent_agent,AgentName)|RestSystemEvents]),
    supervisors(System,Supervisors),
    foreach(member(sup_rel(_,SupSet,_),Supervisors),\+supervised(AgentName,SupSet)),
    !,
    setSystemEvents(System,RestSystemEvents,NewSystem).

%% An agent has diverged, and is being supervised. Try to unblock it.
system_step(System,unblock2,NewSystem,[],Env,Env) :-
    systemEvents(System,[systemEvent(divergent_agent,AgentName)|RestSystemEvents]),
    !,
    setSystemEvents(System,RestSystemEvents,System1),
    supervisors(System1,Supervisors),
    member(SupRel,Supervisors),
    SupRel = sup_rel(SupervisorAgent,SupSet,_SupPolicy),
    supervised(AgentName,SupSet),

    %% Check if name exists in system
    ( agent_through_name(System,AgentName,_) ->
      supervision_divergent(System1,AgentName,SupRel,NewSystem)
    ;
    %% No, let's kill the supervisor
    kill_agent(System1,SupervisorAgent,NewSystem)
    ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Distributed actions:

distAction(action(Action)) :-
    Action =.. [Pred|_],
    member(Pred,['.create_agent','.kill_agent','.monitor','.supervise','.inc_time']).

%% Creating a named agent
distAction_step(ContainerName,Agent,Action,Actions,System,create_agent1,NewSystem,[]) :-
    Action = action('.create_agent'(NewName,Plans,Init)),
    canBeCreated(System,ContainerName,NewName,Plans,Init),
    !,
    actions(Agent,Actions),
    delete(Actions,Action,Actions1),
    setActions(Agent,Actions1,Agent1),
    create_agent_in_system(System,ContainerName,NewName,Plans,Init,System1),
    update_agent(System1,Agent1,NewSystem).
      
%% Killing an agent
distAction_step(_ContainerName,Agent,Action,Actions,System,kill_agent,NewSystem,[]) :-
    Action = action('.kill_agent'(AgentName)),
    !,
    delete(Actions,Action,Actions1),
    setActions(Agent,Actions1,NewAgent),
    kill_agent(System,AgentName,System1),
    update_agent(System1,NewAgent,NewSystem).

distAction_step(_ContainerName,Agent,Action,Actions,System,Rule,NewSystem,[]) :-
    Action = action('.monitor'(Monitored,Persist)),
    delete(Actions,Action,Actions1),
    setActions(Agent,Actions1,Agent1),
    monitors(System,Monitors),
    name(Agent,Monitoring),
    message(normal,'Starting monitoring agent ~w from agent ~w~n',[Monitored,Monitoring]),
    ( member(mon_rel(Monitoring,Monitored,Persist1),Monitors) ->
      Rule=monitor_agent1,
      delete(Monitors,mon_rel(Monitoring,Monitored,Persist1),Monitors1),
      setMonitors(System,[mon_rel(Monitoring,Monitored,Persist)|Monitors1],System1),
      update_agent(System1,Agent1,NewSystem)
    ;
    di(System,Di),
    names(Di,Names),
    ( member(Monitored,Names) ->
      Rule=monitor_agent2,
      Belief = belief(agent_up(Monitored)),
      message(normal,'Belief ~w added to ~w due to monitor.~n',[Belief,Monitoring]),
      addBelief(Agent1,Belief,Agent2),
      events(Agent2,Events),
      setEvents(Agent2,[event(add(Belief),empty)|Events],Agent3),
      ( member(agent_up,Persist) ->
        setMonitors(System,[mon_rel(Monitoring,Monitored,Persist)|Monitors],System1)
      ;
      System1 = System
      ),
      update_agent(System1,Agent3,NewSystem)
    ;
      Rule=monitor_agent3,
      Belief = ann(belief(agent_down(Monitored)),[reason(unknown_agent)]),
      message(normal,'Belief ~w added to ~w due to monitor.~n',[Belief,Monitoring]),
      addBelief(Agent1,Belief,Agent2),
      events(Agent2,Events),
      setEvents(Agent2,[event(add(Belief),empty)|Events],Agent3),
      ( member(agent_down,Persist) ->
        setMonitors(System,[mon_rel(Monitoring,Monitored,Persist)|Monitors],System1)
      ;
      System1 = System
      ),
      update_agent(System1,Agent3,NewSystem)
    )
    ).

distAction_step(_ContainerName,Agent,Action,Actions,System,supervise1,NewSystem,[]) :-
    Action = action('.supervise'(AgentsRecipes,SupPolicy)),
    !,
    delete(Actions,Action,Actions1),
    setActions(Agent,Actions1,Agent1),
    update_agent(System,Agent1,System1),
    name(Agent,ASup),
    ( can_be_started(AgentsRecipes,System1) ->
      message(normal,'Agent ~w acting as supervisor.~n',[ASup]),
      start_agents(System1,AgentsRecipes,System2),
      agent_names(AgentsRecipes,SupAgents),
      supervisors(System2,Supervisors),
      maplist(make_info,SupAgents,SupSet),
      setSupervisors(System2,[sup_rel(ASup,SupSet,SupPolicy)|Supervisors],NewSystem)
    ;
    kill_agent(System1,ASup,NewSystem)
    ).

%% Increment time in local agent
distAction_step(_ContainerName,Agent,Action,Actions,System,inc_time,NewSystem,[]) :-
    Action = action('.inc_time'(Increment)),
    !,
    time(System,Time),
    NewTime is Time+Increment,
    setTime(System,NewTime,System1),
    actions(Agent,Actions),
    delete(Actions,Action,Actions1),
    setActions(Agent,Actions1,NewAgent),
    update_agent(System1,NewAgent,NewSystem).

%% Here we collect all the error cases for distributed actions; for now we just ignore
%% such actions. 
distAction_step(_ContainerName,Agent,Action,Actions,System,distAction_error,NewSystem,[]) :-
    delete(Actions,Action,Actions1),
    setActions(Agent,Actions1,Agent1),
    update_agent(System,Agent1,NewSystem).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

noSystemEvent(System) :-
    systemEvents(System,[]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

isMonitorEvent(systemEvent(Type,_)) :-
    member(Type,[created_agent,unreachable_agent,dead_agent,restarted_agent,unblocked_agent]).

systemEventType(systemEvent(Type,_),agent_up) :-
    member(Type,[created_agent,restarted_agent]), !.
systemEventType(systemEvent(Type,_),agent_down) :-
    member(Type,[unreachable_agent,dead_agent]), !.
systemEventType(systemEvent(Type,_),agent_unblocked) :-
    member(Type,[unblocked_agent]), !.

responsableAgent(systemEvent(_,AgentName),AgentName).

%% Add more cases...
createBelief(systemEvent(created_agent,AgentName),belief(agent_up(AgentName))) :- !.
createBelief(systemEvent(restarted_agent,AgentName),belief(agent_up(AgentName))) :- !.
createBelief(systemEvent(unblocked_agent,AgentName),belief(agent_unblocked(AgentName))) :- !.
createBelief(systemEvent(unreachable_agent,AgentName),ann(belief(agent_down(AgentName)),[reason(unreachable_agent)])) :- !.
createBelief(systemEvent(dead_agent,AgentName),ann(belief(agent_down(AgentName)),[reason(dead_agent)])) :- !.

updateBeliefs(EventType,AgentResp,Belief,System,NewSystem) :-
    monitors(System,Monitors),
    di(System,Di),
    maplist(updateBeliefs(Belief,EventType,AgentResp,Monitors),Di,NewDi),
    setDi(System,NewDi,NewSystem).

updateBeliefs(Belief,EventType,AgentResp,Monitors,{ContainerName,Agents},{ContainerName,NewAgents}) :-
    maplist(updateBeliefsInAgent(Belief,EventType,AgentResp,Monitors),Agents,NewAgents).

updateBeliefsInAgent(Belief,EventType,AgentResp,Monitors,Agent,NewAgent) :-
    name(Agent,AgentName),
    ( member(mon_rel(AgentName,AgentResp,_),Monitors) ->
      events(Agent,Events),
      setEvents(Agent,[event(add(Belief),empty)|Events],Agent1),
      addBelief(Agent1,Belief,NewAgent),
      message(normal,'Added belief ~w to ~w due to monitored event ~w on agent ~w~n',[Belief,AgentName,EventType,AgentResp])
    ;
      NewAgent = Agent).

updateMonitors([],_EventType,_AgentResp,[]).
updateMonitors([Monitor|RestMonitors],EventType,AgentResp,NewMonitors) :-
    Monitor = mon_rel(_Monitoring,Monitored,Persist),
    ( (AgentResp \== Monitored ; member(EventType,Persist)) ->
      updateMonitors(RestMonitors,EventType,AgentResp,Monitors1),
      NewMonitors = [Monitor | Monitors1]
    ;
      updateMonitors(RestMonitors,EventType,AgentResp,NewMonitors)
    ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

actionsEmpty({_,Agents}) :-
    foreach(member(Agent,Agents),actions(Agent,[])).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

system_terminated(System,Env) :-
    di(System,Containers),
    %% foreach(member({_,Agents},Containers),Agents==[]).
    forall(member({_,Agents},Containers),
           forall(member(Agent,Agents),
                  ( agent_terminated(Agent,Env),
                    step(Agent,Step),
                    \+ member(Step,[diverges,stopping])
                  ))),
    systemEvents(System,[]),
    outMessages(System,Messages),
    forall(member(msg(_,ReceiverName,_,_),Messages),\+agent_through_name(System,ReceiverName,_)).

system_stopped(Sys,Env) :-
     di(Sys,Containers),
     forall(member({_,Agents},Containers),
            forall(member(Agent,Agents),agent_terminated(Agent,Env))),
     outMessages(Sys,Messages),
     forall(member(msg(_,ReceiverName,_,_),Messages),\+agent_through_name(Sys,ReceiverName,_)).

system_printer(Stream,System) :-
    di(System,Di),
    foreach(member(Runtime,Di),runtime_printer(Stream,Runtime)),
    systemEvents(System,SystemEvents),
    monitors(System,Monitors),
    supervisors(System,Supervisors),
    time(System,Time),
    (SystemEvents \== [] -> format(Stream,'time=~w system events: ~w~n',[Time,SystemEvents]); format(Stream,'time=~w~n',[Time])),
    (Monitors \== [] -> format(Stream,'monitors=~w~n',[Monitors]); true),
    (Supervisors \== [] -> format(Stream,'supervisors=~w~n',[Supervisors]); true).

runtime_printer(Stream,{ConfigurationName,Agents}) :-
    format(Stream,'runtime ~w:~n',[ConfigurationName]),
    foreach(member(Agent,Agents),agent_printer(Stream,Agent)),
    !.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

systemParams(Params) :-
    stdParams(Std),
    setTransRel(Std,system_step,system_terminated,system_printer,Params).

run_system(SystemPred,History) :-
    run_system(SystemPred,_,History,[]).

run_system(SystemPred,FinalSystem,History) :-
    run_system(SystemPred,FinalSystem,History,[]).

run_system(SystemPred,FinalSystem,History,ExtraParams) :-
    apply(SystemPred,[System]),
    systemParams(PreParams),
    append(ExtraParams,PreParams,Params),
    ( member({bench,N},Params) ->
      Goal = 
      (
          call_time(runTerminate(System,FinalSystem,History,Params,_),Dict),
          get_dict(wall,Dict,WallTime),
          countAgentSteps(History,NumSteps),
          Result = _{wall:WallTime,steps:NumSteps}
      ),
      repeatN(N,{Goal,Result},Results),
      report_results(Results)
    ;
    runTerminate(System,FinalSystem,History,Params,_)
    ).

report_results(L) :-
    predsort(wallPred,L,SortedL),
    %% Skip slowest and quickest run
    [_|RestSorted] = SortedL,
    reverse(RestSorted,[_|RestReversed]),
    reverse(RestReversed,Results),
    length(Results,N),
    % Calculate medium for wall times and number of rules
    medium(Results,N,Wall,Rules),
    format('Rule: ~w Wall: ~w~n',[Rules,Wall]).

medium(Results,N,Wall,Rules) :-
    medium(Results,N,[],[],Wall,Rules).
medium([],N,Walls,Rules,Wall,NumRules) :-
    sumlist(Walls,SumWalls),
    Wall is SumWalls / N,
    sumlist(Rules,SumRules),
    NumRules is SumRules / N.
medium([Result|Rest],N,Walls,Rules,Wall,NumRules) :-
    get_dict(wall,Result,WallTime),
    get_dict(steps,Result,CountRules),
    medium(Rest,N,[WallTime|Walls],[CountRules|Rules],Wall,NumRules).

wallPred(Cmp,R1,R2) :-
    get_dict(wall,R1,T1),
    get_dict(wall,R2,T2),
    compare(Cmp,T1,T2).

repeatN(0,_,[]) :-
    !.
repeatN(N,{Goal,Result},[NewResult|Results]) :-
    copy_term({Goal,Result},{NewGoal,NewResult}),
    NewGoal,
    !,
    N1 is N-1,
    repeatN(N1,{Goal,Result},Results).

countAgentSteps([],0) :- !.
countAgentSteps([step(_,_,_)|Rest],N) :-
    !,
    countAgentSteps(Rest,N1),
    N is N1+1.
countAgentSteps([_|Rest],N) :-
    countAgentSteps(Rest,N).
