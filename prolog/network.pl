%% Network semantics, i.e., a non-connected collection of multi-agent systems.
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

%% Rules for merging two MAS into a single one
%% and for splitting a MAS in two go here, and a rule
%% for evolving a single multi-agent system.

:- ensure_loaded(base). %% disjoint, union, ...


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Basic step rule for networks.

network_step(Networks,merge,[System|NewNetworks],[],Env,Env) :-
    member(System1,Networks),
    member(System2,Networks),
    System1 \== System2,
    merge(System1,System2,System),
    delete(Networks,System1,Networks1),
    delete(Networks1,System2,NewNetworks).

network_step(Networks,Rule,[System|NewNetworks],Actions,Env,NewEnv) :-
    randomized_member(System1,Networks),
    system_step(System1,SystemRule,System2,Actions,Env,NewEnv),
    ( SystemRule = [_|_] ->
      Rule = [distribution_int | SystemRule]
    ; Rule = [distribution_int, SystemRule]
    ),
    inc_time(System2,System),
    delete(Networks,System1,NewNetworks).

network_step(Networks,split1,[System1,System2|NewNetworks],[],Env,Env) :-
    member(System,Networks),
    split1(System,[System1,System2]),
    delete(Networks,System,NewNetworks).

network_step(Networks,split2,[NewSystem|NewNetworks],[],Env,Env) :-
    member(System,Networks),
    split2(System,[NewSystem]),
    delete(Networks,System,NewNetworks).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% MERGE
%% merge/3
%% merge(System1,System2,MergedSystem)
merge(sys(Di1,Mo1,Su1,SE1,T1),sys(Di2,Mo2,Su2,SE2,T2),sys(Di,Mo,Su,SE,T)) :-
    names(Di1,N1),
    names(Di2,N2),
    disjoint(N1,N2),
    containerNames(Di1,CN1),
    containerNames(Di2,CN2),
    disjoint(CN1,CN2),
    union(Di1,Di2,Di),
    union(Mo1,Mo2,Mo),
    union(Su1,Su2,Su),
    append(SE1,SE2,SE12),
    names(Di,N),
    creationevents(N,CreationEvents),
    append(SE12,CreationEvents,SE),
    inc_time(T1,T2,T).

%% creationevents/2 generates a sequence of created_agent() events
creationevents([],[]).
creationevents([Name|Names],[systemEvent(created_agent,Name)|Events]) :-
    creationevents(Names,Events).

%% SPLIT
%% split/2
%% split(System,[System1,System2])

%% mo_from_mo_di/3
%% mo_from_mo_di(Mo,Di,Mo')
%% filters monitoring relations according to a set of names in a given container
mo_from_mo_di(Mo,Di,Mo1) :-
    names(Di,DiN),
    findall(mon_rel(Monitoring,Monitored,Persist),
	    (member(mon_rel(Monitoring,Monitored,Persist),Mo),member(Monitoring,DiN)),
	    Mo1).

%% su_from_su_di/3
%% su_from_su_di(Su,Di,Su')
%% similar to mo_from_mo_di/3 but for supervision relations
su_from_su_di(Su,Di,Su1) :-
    names(Di,DiN),
    findall(sup_rel(Supervisor,SupSet,SupPolicy),
	    (member(sup_rel(Supervisor,SupSet,SupPolicy),Su),
	     member(Supervisor,DiN),
	     checknames(SupSet,DiN)),
	    Su1).

%% checknames/2
%% checknames(SupSet,Names)
%% for every (name,...) in SupSet, name in Names
checknames([],_Names).
checknames([(Agent,_,_,_)|SupSet],Names) :-
    member(Agent,Names),
    checknames(SupSet,Names).

%% se_from_se_di/2
%% se_from_se_di(SE,Di,SE')
%% similar to mo_from_mo_di/3 but for system events
se_from_se_di(SE,Di,SE1) :-
    names(Di,DiN),
    map_unreachable(DiN,UnreachableEvents),
    append(SE,UnreachableEvents,SE1).

map_unreachable([],[]).
map_unreachable([Agent|Agents],
                [systemEvent(unreachable_agent,Agent),systemEvent(stopped_agent,Agent)|UnreachableEvents]) :-
    map_unreachable(Agents,UnreachableEvents).


%% Split is quite destructive for supervision.
%% Conside the ag1,...,agn in a supervision "set", and the supervising agent sag,
%% if among these agents there are agents that belong to different systems after the split,
%% then:
%% - the supervision relation is removed
%% - the supervisor agent is killed
%% - all agents ag1,...,agn are killed

%% SPLIT1
split1(sys(Di,Mo,Su,SE,T),[Sys1,Sys2]) :-
    Sys1Pre = sys(Di1,Mo1,Su1,SE1,T),
    Sys2Pre = sys(Di2,Mo2,Su2,SE2,T),
    divide(Di,Di1,Di2), Di1 \= [], Di2 \= [],

    %% Find split supervisor relations
    include(localSupervisor(Sys1Pre),Su,SuLocal1),
    exclude(localSupervisor(Sys1Pre),Su,NonLocalSu1),
    include(localSupervisor(Sys2Pre),Su,SuLocal2),
    exclude(localSupervisor(Sys2Pre),Su,NonLocalSu2),

    mo_from_mo_di(Mo,Di1,Mo1),
    mo_from_mo_di(Mo,Di2,Mo2),
    su_from_su_di(SuLocal1,Di1,Su1),
    su_from_su_di(SuLocal2,Di2,Su2),
    se_from_se_di(SE,Di1,SE2),
    se_from_se_di(SE,Di2,SE1),

    % All agents in non-local supervisors
    maplist(localNamesInSupRel(Di1),NonLocalSu1,AgentsInSu1),
    flatten(AgentsInSu1,AllAgentsToKill1),
    maplist(localNamesInSupRel(Di2),NonLocalSu2,AgentsInSu2),
    flatten(AgentsInSu2,AllAgentsToKill2),

    %% Kill agents
    kill_agents(AllAgentsToKill1,Sys1Pre,Sys1),
    kill_agents(AllAgentsToKill2,Sys2Pre,Sys2).

%% SPLIT2
split2(sys(Di,Mo,Su,SE,T),[SysSplit]) :-
    SysSplitPre = sys(Di1,Mo1,Su1,SE1,T),
    divide(Di,Co,Di1), Co \= [],

    %% Find split supervisor relations
    include(localSupervisor(SysSplitPre),Su,SuLocal),
    exclude(localSupervisor(SysSplitPre),Su,NonLocalSu),

    su_from_su_di(SuLocal,Di1,Su1),
    mo_from_mo_di(Mo,Di1,Mo1),
    names(Co,CoN),
    map_dead(CoN,DeadEvents),
    append(SE,DeadEvents,SE1),

    % All agents in split supervisors
    maplist(localNamesInSupRel(Di1),NonLocalSu,AgentsInSu),
    flatten(AgentsInSu,AllAgentsToKill),

    %% Kill agents
    kill_agents(AllAgentsToKill,SysSplitPre,SysSplit).
    
localSupervisor(Sys,SupRel) :-
    agentsInSu(SupRel,AgentNames),
    names(Sys,NamesInSys),
    forall(member(Name,AgentNames),member(Name,NamesInSys)).

localNamesInSupRel(Di,SupRel,Names) :-
    names(Di,DiNames),
    agentsInSu(SupRel,AgentNames),
    include(localName(DiNames),AgentNames,Names).

localName(LocalNames,Name) :-
    member(Name,LocalNames).

agentsInSu(sup_rel(AgentName,SupSet,_),[AgentName|AgentNames]) :-
    maplist(agentNameFromSup,SupSet,AgentNames).

map_dead([],[]).
map_dead([Agent|Agents],
         [systemEvent(dead_agent,Agent),systemEvent(stopped_agent,Agent)|DeadEvents]) :-
    map_dead(Agents,DeadEvents).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

inc_time(System,NewSystem) :-
    time(System,Time),
    inc_time1(Time,NewTime),
    setTime(System,NewTime,NewSystem).

inc_time1(Time,NewTime) :-
    NewTime is Time+1.
    
inc_time(Time1,Time2,NewTime) :-
    (Time1 > Time2 -> inc_time1(Time1,NewTime) ; inc_time1(Time2,NewTime)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

network_printer(Stream,Network) :-
    foreach(member(System,Network),(format(Stream,'network:~n',[]),system_printer(Stream,System))),
    !.

network_terminated(Network,Env) :-
    %% This is too restrictive.
    %% On the other hand, this is in a sense a
    %% heuristic, so if we want to explore that scenario we just use a network_terminated which
    %% is more precise.
    foreach(member(System,Network),system_terminated(System,Env)),
    !.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

networkParams(Params) :-
    stdParams(Std),
    setTransRel(Std,network_step,network_terminated,network_printer,Params).

run_network(NetworkPred,History) :-
    run_network(NetworkPred,_,History,[]).

run_network(NetworkPred,FinalNetwork,History,ExtraParams) :-
    apply(NetworkPred,[Network]),
    networkParams(PreParams),
    append(ExtraParams,PreParams,Params),
    runTerminate(Network,FinalNetwork,History,Params,_).
