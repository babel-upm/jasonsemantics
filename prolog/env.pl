%% Environment handling code including percepts handling
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

%% Env: env(State,Perceiver)

updateBeliefs(Agent,OldPercepts,NewPercepts,AgentNew) :-
    difference(OldPercepts,NewPercepts,MissingPercepts),
    difference(NewPercepts,OldPercepts,FreshPercepts),
    maplist(new_belief,FreshPercepts,NewBeliefs),
    maplist(missing_belief,MissingPercepts,MissingBeliefs),
    append(MissingBeliefs,NewBeliefs,AllBeliefs),
    ( MissingBeliefs == [], NewBeliefs == [] -> true ;
     name(Agent,Name),
     message(normal,'Agent ~w perceived; new beliefs ~w missing old beliefs ~w~n',[Name,NewBeliefs,MissingBeliefs])
    ),
    updateBeliefs1(AllBeliefs,Agent,AgentNew).

updateBeliefs1([],Agent,Agent).
updateBeliefs1([Update|Rest],Agent,NewAgent) :-
    updateBelief(Update,Agent,Agent1),
    updateBeliefs1(Rest,Agent1,NewAgent).
updateBelief(del(Belief),Agent,NewAgent) :-
    addEvents(Agent,[event(del(Belief),empty)],Agent1),
    beliefs(Agent1,Beliefs),
    delete(Beliefs,Belief,Beliefs1),
    setBeliefs(Agent1,Beliefs1,NewAgent).
updateBelief(add(Belief),Agent,NewAgent) :-
    addEvents(Agent,[event(add(Belief),empty)],Agent1),
    beliefs(Agent1,Beliefs),
    setBeliefs(Agent1,[Belief|Beliefs],NewAgent).

difference([],_,[]).
difference([First|Rest],L,Result) :-
    member(First,L), !, difference(Rest,L,Result).
difference([First|Rest],L,[First|Result]) :-
    difference(Rest,L,Result).

initializeBeliefs(Agent,Percepts,NewAgent) :-
    maplist(new_belief,Percepts,NewBeliefs),
    initializeBeliefs1(NewBeliefs,Agent,NewAgent).

initializeBeliefs1([],Agent,Agent).
initializeBeliefs1([Initialize|Rest],Agent,NewAgent) :-
    initializeBelief(Initialize,Agent,Agent1),
    initializeBeliefs1(Rest,Agent1,NewAgent).
initializeBelief(del(Belief),Agent,NewAgent) :-
    beliefs(Agent,Beliefs),
    delete(Beliefs,Belief,Beliefs1),
    setBeliefs(Agent,Beliefs1,NewAgent).
initializeBelief(add(Belief),Agent,NewAgent) :-
    beliefs(Agent,Beliefs),
    setBeliefs(Agent,[Belief|Beliefs],NewAgent).

new_belief(belief(Belief),add(ann(belief(Belief),source(percept)))).
missing_belief(belief(Belief),del(ann(belief(Belief),source(percept)))).

modifyEnv([],Env,Env).
modifyEnv([action(Action)|Rest],Env,NewEnv) :-
    Action =.. [ActionFunctor|ActionArgs],
    modifyEnv(ActionFunctor,ActionArgs,Env,Env1),
    modifyEnv(Rest,Env1,NewEnv).
modifyEnv('.supervise',_,Env,Env) :- !.
modifyEnv('.monitor',_,Env,Env) :- !.
modifyEnv('.print',_,Env,Env) :- !.
modifyEnv('.send',_,Env,Env) :- !.
modifyEnv('.inc_time',_,Env,Env) :- !.
modifyEnv(Pred,Args,Env,NewEnv) :-
    Env = env(State,Perceiver),
    append(Args,[State,NewState],PredArgs),
    ( apply(Pred,PredArgs) *->
      message(debug,'modifyEnv ~w with args ~w in state ~w returned new state ~w~n',[Pred,Args,State,NewState]),
      NewEnv = env(NewState,Perceiver)
    ;
    message(error,'modifyEnv ~w with args ~w in state ~w FAILED~n',[Pred,PredArgs,State]),
    abort
    ).

empty_env(env(empty_env,_)).
no_perceive(_X,[]).

    
