%% Auxiliary predicates
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

divide(S,S1,S2) :-
    bsubset(S1,S),
    S1 \== [],
    deleteAll(S1,S,S2),
    S2 \== [].

deleteAll([],S,S).
deleteAll([First|Rest],S,NewS) :-
    delete(S,First,S1),
    deleteAll(Rest,S1,NewS).

bsubset([],_).
bsubset([Element|Rest],L) :-
    member(Element,L),
    delete(L,Element,L1),
    bsubset(Rest,L1).

%% agents2/2 extracts agent names from agent configurations
%% agents2([],[]).
%% agents2([ac(_,Name,_,_,_,_)|ACs],[Name|Names]) :-
%%    agents2(ACs,Names).

%% Removing duplicates, etc
%% nodups/2 removes duplicates from a SORTED list
%% nodups(Sorted,SortedNoDups)
nodups([],[]).
nodups([X],[X]).
nodups([A,B|Cs],[A|Ys]) :-
    A \= B,
    nodups([B|Cs],Ys).
nodups([A,B|Cs],Ys) :-
    A = B,
    nodups([B|Cs],Ys).
%% UNNECESSARY!!!!! sort/2 already removes duplicates!!!!!!!!!!!!!

%% set difference
%% setminus/3
%% setminus(As,Bs,Cs)
%% As, Bs, Cs are sets represented as ordered lists without duplicates
%% Cs is the set difference As \ Bs
setminus([],_,[]).
setminus([H|Ts],[],[H|Ts]).
setminus([X|As],[X|Bs],Cs) :-
    setminus(As,Bs,Cs).
setminus([A|As],[B|Bs],[A|Cs]) :-
    A @< B,
    setminus(As,[B|Bs],Cs).
setminus([A|As],[B|Bs],Cs) :-
    B @< A,
    setminus([A|As],Bs,Cs).

%% disjoint sets
%% disjoint/2
%% disjoint(Xs,Ys) -- Xs interseccion Ys = 0
%% |Xs ++ Ys| = |Xs U Ys|
disjoint([],_).
disjoint([_|_],[]).
disjoint([H|Ts],[X|Ys]) :-
    H @< X,
    disjoint(Ts,[X|Ys]).
disjoint([H|Ts],[X|Ys]) :-
    X @< H,
    disjoint([H|Ts],Ys).

%% set union
%% union/3
%% union(Xs,Ys,Zs) -- Zs is Xs U Ys
union(Xs,Ys,Zs) :-
    append(Xs,Ys,Ws),
    sort(Ws,Zs).

%% randomized member, but with choice points
randomized_member(X,L) :-
    random_permutation(L,LR),
    !,
    member(X,LR).
