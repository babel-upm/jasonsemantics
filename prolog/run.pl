%% The jason interpreter
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

%%
%% Params: params: [outLogLevel(Level),logLogLevel(Level),log(Stream),stepping,step(Stepper),printer(Printer),terminated(Checker),guide(Guide)]
%%

:- dynamic outLogLevel/1.
:- dynamic logLogLevel/1.
:- dynamic loggingTo/1.

runTerminate(Config,FinalConfig,ReturnedStructure,Params,FinalEnv) :-
    param(outLogLevel(OutLevel),Params),
    param(logLogLevel(LogLevel),Params),
    ( outLogLevel(_) -> retract(outLogLevel(_)) ; true ),
    ( logLogLevel(_) -> retract(logLogLevel(_)) ; true ),
    ( loggingTo(_) -> retract(loggingTo(_)) ; true ),
    assert(outLogLevel(OutLevel)),
    assert(logLogLevel(LogLevel)),
    ( member(log(Stream),Params) -> assert(loggingTo(Stream)) ; true ),
    
    param(initial_env(InitialEnv),Params),

    info(Params,'Starting run in configuration ~w~n~n',[Params]),
    printer(info,Params,Config),

    runTerminate(Config,[Config],History,FinalConfig,Params,_,InitialEnv,FinalEnv),
    %% This does not work due to backtracking: ( member(log(Stream),Params) -> close(Stream) ; true ).
    ( member(returnActions,Params) ->
      actions_history(History,ReturnedStructure)
    ;
    ReturnedStructure = History
    ),
    true.

runTerminate(Config,History,History,Config,Params,_,Env,FinalEnv) :-
    terminated(Config,Params,Env),
    !,
    normal(Params,'~n~nFinal term:~n'),
    printer(normal,Params,Config),
    (\+ empty_env(FinalEnv) -> normal('Final env: ~w~n',[FinalEnv]) ; true),
    normal(Params,'~n~n'),
    %%print_chpoint,
    true.

runTerminate(Config,History,FinalHistory,FinalConfig,Params,Rule,Env,FinalEnv) :-
    %% Execute a transition step which is accepted by the (optional) guide
    ( ( step(Config,Rule,NextConfig,Actions,Params,Env,Env1),

        %% If the execution is guided check that the guide predicate, in its current state,
        %% permits the state transition
        ( member(guide(Guide,State),Params) ->
          apply(Guide,[State,Config,Rule,Env,NextConfig,NextState]),
          ( NextState==stop -> Stopping = true ; Stopping = false ),
          delete(Params,guide(Guide,State),Params1),
          NextParams=[guide(Guide,NextState)|Params1]
        ;
        Stopping = false, NextParams = Params
        )
      ) *->
      
      %% Transition executed correctly

      NextHistory = [step(Rule,NextConfig,Actions)|History],
      modifyEnv(Actions,Env1,NewEnv),

      (Actions \== [] -> message(info,'Actions are ~w~n',[Actions]) ; true),
      message(fine_debug,'Old Env=~w NewEnv=~w~n',[Env1,NewEnv]),
      (Env1 \== NewEnv -> NewEnv=env(State,_), message(normal,'Env: ~w~n',[State]) ; true),
      info(Params,'~nran rule ~w~n~n',[Rule]),
      printer(info,Params,NextConfig),
      (\+ empty_env(NewEnv) -> info(Params,'Env: ~w~n',[NewEnv]) ; true ),
      ( member(guide(_,State),Params) -> info(Params,'Guide state: ~w~n',[NextState]) ; true ),
      info(Params,'~n'),

      ( Stopping = true ->
        FinalConfig = NextConfig,
        FinalHistory = NextHistory,
        FinalEnv = NewEnv
      ;
      (stepping(Params) -> doCreep(NextConfig,NextRule) ; true),
      ( NextRule==halt -> true ; runTerminate(NextConfig,[step(Rule,NextConfig,Actions)|History],FinalHistory,FinalConfig,NextParams,NextRule,NewEnv,FinalEnv) )
      )
    ;
    actions_history(History,Actions),
    error(Params,'Run terminated; actions=~w; no step ~w for configuration: ~n',[Actions,Rule]),
    printer(error,Params,Config),
    doCreep(Config,NextRule),
    ( NextRule==halt -> true ; runTerminate(Config,History,FinalHistory,FinalConfig,Params,NextRule,Env,FinalEnv) )
    ).

actions_history([],[]) :- !.
actions_history([step(_,_,StepActions)|Rest],Actions) :-
    !,
    actions_history(Rest,RestActions),
    append(StepActions,RestActions,Actions).
actions_history([_|Rest],Actions) :-
    actions_history(Rest,Actions).

step(Config,Rule,NextConfig,Actions,Params,Env,NewEnv) :-
    message(fine_debug,'step(Env=~w NewEnv=~w Actions=~w)~n',[Env,NewEnv,Actions]),
    param(step(Stepper),Params),
    apply(Stepper,[Config,Rule,NextConfig,Actions,Env,NewEnv]),
    message(fine_debug,'step(Env=~w NewEnv=~w Actions=~w) Rule was ~w~n',[Env,NewEnv,Actions,Rule]).

terminated(Config,Params,Env) :-
    param(terminated(Checker),Params),
    apply(Checker,[Config,Env]).

param(Term,Params) :-
    member(Term,Params), !.
param(Term,Params) :-
    format('~n*** ERROR: required parameter ~w not set in ~w~n',[Term,Params]),
    fail.

doCreep(Config,NextRule) :-
    format('~nname of rule to apply (halt to terminate, carriage return to let prolog choose): '),
    read_line_to_string(user_input,Cmd),
    doCreep(Cmd,Config,NextRule).
doCreep("",_Config,_) :-
    true.
doCreep(Cmd,_Config,NextRule) :-
    atom_string(NextRule,Cmd).

stepping(Params) :-
    member(stepping,Params).

%% For debugging
print_chpoint :-
   prolog_current_choice(ChI),
   prolog_choice_attribute(ChI,frame,F),
   prolog_frame_attribute(F,goal,Goal),
   message(debug,'Last choice point: ~w~n', [ Goal ]).

printer(Level,Params,Config) :-
    param(printer(Printer),Params),
    param(outLogLevel(OutLevel),Params),
    ( hasLevel(Level,OutLevel) -> apply(Printer,[current_output, Config]) ; true ),
    param(logLogLevel(LogLevel),Params),
    ( member(log(Stream),Params), hasLevel(Level,LogLevel) -> apply(Printer,[Stream,Config]) ; true ).
                                  
info(Params,FormatString) :-
    message(info,Params,FormatString,[]).
info(Params,FormatString,Args) :-
    message(info,Params,FormatString,Args).

warning(Params,FormatString) :-
    warning(Params,FormatString,[]).
warning(Params,FormatString,Args) :-
    message(warning,Params,FormatString,Args).

error(Params,FormatString) :-
    error(Params,FormatString,[]).
error(Params,FormatString,Args) :-
    message(error,Params,FormatString,Args).

normal(Params,FormatString) :-
    message(normal,Params,FormatString,[]).
normal(Params,FormatString,Args) :-
    message(normal,Params,FormatString,Args).

hasLevel(Level1,Level2) :-
    level(Level1,Num1),
    level(Level2,Num2),
    Num1 >= Num2.

level(fine_debug,1).
level(debug,5).
level(info,10).
level(normal,11).
level(warning,12).
level(error,13).    

message(Level,PreFormatString,Args) :-
    format_message(Level,PreFormatString,FormatString),
    ( outLogLevel(OutLevel) -> true ; format('~n*** ERROR: no outLogLevel set.~n',[]), fail ),
    ( hasLevel(Level,OutLevel) -> format(current_output,FormatString,Args) ; true ),
    ( logLogLevel(LogLevel) -> true ; format('~n*** ERROR: no logLogLevel set.~n',[]), fail ),
    ( loggingTo(Stream), hasLevel(Level,LogLevel) -> format(Stream,FormatString,Args) ; true ).

message(Level,Params,PreFormatString,Args) :-
    format_message(Level,PreFormatString,FormatString),
    param(outLogLevel(OutLevel),Params),
    ( hasLevel(Level,OutLevel) -> format(current_output,FormatString,Args) ; true ),
    param(logLogLevel(LogLevel),Params),
    ( member(log(Stream),Params), hasLevel(Level,LogLevel) -> format(Stream,FormatString,Args) ; true ).
    
format_message(warning,PreMsg,Msg) :-
    string_concat('~n*** WARNING: ',PreMsg,Msg), !.
format_message(error,PreMsg,Msg) :-
    string_concat('~n*** ERROR: ',PreMsg,Msg), !.
format_message(_,Msg,Msg).

stdParams(Params) :-
    Params = [outLogLevel(normal),logLogLevel(normal),initial_env(env(empty_env,no_perceive))].
logToFile(Params,FileName,NewParams) :-
    open(FileName, write, Stream, []),
    NewParams = [log(Stream) | Params].
stepping(Params,[stepping|Params]).
setTransRel(Params,Stepper,TerminatedChecker,Printer,NewParams) :-
    NewParams = [step(Stepper),printer(Printer),terminated(TerminatedChecker)|Params].
  
