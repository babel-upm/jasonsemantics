%% Main file to load the Jason interpreter
%%
%% Lars-Ake Fredlund, Julio Mariño (November 2022)

:- consult('base.pl').
:- consult('boilerplate.pl').
:- consult('agent.pl').
:- consult('distribution.pl').
:- consult('run.pl').
:- consult('network.pl').
:- consult('supervisor.pl').
:- consult('examples.pl').
:- consult('env.pl').
:- consult('robot_example.pl').
:- consult('tests.pl').
