# An implementation of the Jason operational semantics rules

This code is part of a project to mechanize the semantics of Jason, 
a well-known multi-agent system programming language, together with extensions
from eJason, which adds a distribution and fault-tolerance on top
of the basic Jason language.

The semantics provdides an interpreter for Jason programs, written
in Prolog (we use SWI-Prolog, version 8.5.20). The interpreter faithfully
implements the semantics described in the book Programming Multi-Agent Systems
in AgentSpeak using Jason by Rafael H. Bordini, Jomi Fred Hübner, and Michael
Wooldridge (Wiley, 2007). Moreover it implements the distribution,
supervisor and monitoring concepts described in Álvaro Fernández Díaz's PhD thesis: 
eJason: a Framework for Distributed and Fault-tolerant Multi-agent Systems. PhD thesis
(Escuela Técnica Superior de Ingenieros Informáticos, Universidad Politécnica de Madrid, 2018).

Apart from extending the basic Jason semantics with distribution and
fault-tolerance, we also provide a semantic definition, and implementation
by means of these semantics rules,
of the environment concept, something which was missing in the 
book mentioned above. This permits us to e.g. run the robot example provided
in the Jason book.

The interpreter is quite simple: it merely executes the operational
semantic rules defined in the above book (and Fernandez Diaz's PhD thesis).
Through inspecting the execution history provided by the interpreter we
can examine the details of exactly which semantics rules were applied,
and how the multi-agent system being run evolved, step-by-step.

There are three semantics relations provided, permitting
to execute an agent in isolation, a multi-agent system composed
of a set of agents, or a network consisting of a collection of non-connected
(but which can become connected) multi-agent systems.

Note that the semantics properly implements the choices in a Jason programming
as Prolog choicepoints, meaning that we can e.g. use the Prolog setof predicate
to examine all examinations of a Jason program (the file tests.pl contains
an example of doing so). Note, however, that the Jason multi-agent semantics
is very non-deterministic; it is unlikely that the computation of
all program runs for anything but a quite trivial multi-agent system
will terminate in a reasonable time.

Note that the implementation of the semantics rules is quite inefficient considered
as a general purpose programming language; a more efficient
implementation of eJason is available at
[github.com/avalor/eJason](https://github.com/avalor/eJason).

## Example

To get started with using the Jason interpreter try this out (in the prolog
subdirectory):
````
> swipl

Welcome to SWI-Prolog (threaded, 64 bits, version 8.5.20)
SWI-Prolog comes with ABSOLUTELY NO WARRANTY. This is free software.
Please run ?- license. for legal details.

For online help and background, visit https://www.swi-prolog.org
For built-in help, use ?- help(Topic). or ?- apropos(Word).

?- consult('load.pl').
true.

? %% First lets rune some tests:

run_tests.
% PL-Unit: lists ......
*** WARNING: No relevant plan exists for event event(add(belief(ann(agent_down(hello_agent),[reason(unknown_agent)]))),empty) in agent n
..
*** WARNING: No relevant plan exists for event event(del(belief(ann(world_flat,agent1))),empty) in agent agent2
.....
*** WARNING: No relevant plan exists for event event(add(belief(ann(agent_down(hello_agent),[reason(unknown_agent)]))),empty) in agent n
.. done
% All 15 tests passed
true.

?- %% Next lets run the robot example with a bit more debugging output:

tstRobotRun(Actions).

Agent owner handling event event(add(achieve(goal(get(beer)))),empty)
Agent owner commited to achieving goal get(beer) through plan owner_get_beer
Agent owner sent a message msg(mid(owner,0),robot,achieve,goal(has(owner,beer))) to agent robot
Agent owner achieved goal get(beer) through plan owner_get_beer
Agent robot handling event event(add(achieve(goal(has(owner,beer)))),empty)
Agent robot commited to achieving goal has(owner,beer) through plan robot_has_beer_1
...
Final term:
runtime runtime:
time=52 system events: [systemEvent(dead_agent,owner),systemEvent(dead_agent,supermarket),systemEvent(dead_agent,robot)]


Actions = [action('.print'("beer limit exceeded")), action(owner_sip(beer)), 
action(robot_hand_in_beer(beer)), action(robot_move_towards(owner)), 
action(robot_close(fridge)), action(robot_get(beer)), action(robot_open(fridge)), 
action(robot_move_towards(...)), action(...)|...] 
````
The predicate tstRobotRun(Actions) -- which simply starts the Jason interepreter 
with the Jason robot example loaded --
unifies the Action variable with the sequence of
"actions" executed during the running of robot example. It moreover
prints outs a number of noteworthy events during the execution
of the multi-agent system (the verbosity of print-outs can be controlled
through interpreter options).

## Missing

The most serious omission currently is the lack of a Jason parser. 
Programs must instead be entered as prolog terms in an AST like notation.

Also missing currently is support for mechanisms which implement fault tolerance
__inside__ a Jason agent. There is support for managing fault tolerance inside
a multi-agent system, through the introduction of the monitor and supervisor 
concepts.

Moreover, a lot of the standard Jason actions and predicates are missing.

## File summary

| File | Contents |
| ---- | -------- |
| prolog/ |			files for the mechanization of the Jason semantics and the distribution and fault-tolerance extension in Prolog |
| |
| prolog/load.pl           | loads the rest of the files |
| prolog/examples.pl |       smaller examples |
| prolog/robot_example.pl | the robot example |
| prolog/tests.pl          | a number of tests |
| |
| prolog/agent.pl          | semantics for agents |
| prolog/distribution.pl   | semantics for multi-agent systems (MAS) |
| prolog/network.pl        | semantics for collections of MAS |
| prolog/run.pl            | the Jason interpreter |
| prolog/env.pl            | environment handling code |
| prolog/supervisor.pl     | supervisor and monitor code |
| |
| prolog/boilerplate.pl    | boilerplate code |
| prolog/base.pl		   | some basic predicates |


